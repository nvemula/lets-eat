#django
from django.shortcuts import render_to_response, get_object_or_404,HttpResponse
from django.http import HttpResponseRedirect, Http404
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django.core.urlresolvers import reverse

from recipes.models import Recipe
from restaurants.models import Restaurant

def homeview(request):
    """Displays the landing page of the website and if the user is logged in,shows his specific homeview"""
    recentrecipes = Recipe.objects.all().order_by("-added")[0:3] 
    recentres = Restaurant.objects.all().order_by("-added")[0:3]
    if request.user.is_authenticated(): 
       return HttpResponseRedirect(reverse("recipes.views.recipes"))
       """ return render_to_response("homeview.html",{
       "recipes": recentrecipes,
       "recentres":recentres,
       },context_instance=RequestContext(request))"""     
    else:
         return render_to_response("landing.html",{
         "recipes": recentrecipes,   
         "recentres":recentres,
         },context_instance=RequestContext(request))  
