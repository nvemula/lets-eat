# -*- coding: utf-8 -*-
# Django settings for basic pinax project.

import os.path
import posixpath
from django.core.urlresolvers import reverse


PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

DEBUG = False
TEMPLATE_DEBUG = DEBUG

# tells Pinax to serve media through the staticfiles app.
SERVE_MEDIA = DEBUG

# django-compressor is turned off by default due to deployment overhead for
# most users. See <URL> for more information
COMPRESS = False

INTERNAL_IPS = [
    "127.0.0.1",
]

ADMINS = [
    # ("Your Name", "your_email@domain.com"),
]

MANAGERS = ADMINS

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3", # Add "postgresql_psycopg2", "postgresql", "mysql", "sqlite3" or "oracle".
        "NAME": "dev.db",                       # Or path to database file if using sqlite3.
        "USER": "",                             # Not used with sqlite3.
        "PASSWORD": "",                         # Not used with sqlite3.
        "HOST": "",                             # Set to empty string for localhost. Not used with sqlite3.
        "PORT": "",                             # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = "US/Eastern"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-us"

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, "media")


# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = "/media/"

# Absolute path to the directory that holds static files like app media.
# Example: "/home/media/media.lawrence.com/apps/"
STATIC_ROOT = os.path.join(PROJECT_ROOT,"static")


# URL that handles the static files like app media.
# Example: "http://media.lawrence.com"
STATIC_URL = "/static/"

# Additional directories which hold static files
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, "static"),
]

STATICFILES_FINDERS = [
    "staticfiles.finders.FileSystemFinder",
    "staticfiles.finders.AppDirectoriesFinder",
    "staticfiles.finders.LegacyAppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
]

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = posixpath.join(STATIC_URL, "admin/")

# Subdirectory of COMPRESS_ROOT to store the cached media files in
COMPRESS_OUTPUT_DIR = "cache"

# Make this unique, and don't share it with anybody.
SECRET_KEY = "jg0(^b^xmao7(pm2+2fzah%v@s2bg$2i+rf#xe23r@^q%s2)y5"

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = [
    "django.template.loaders.filesystem.load_template_source",
    "django.template.loaders.app_directories.load_template_source",
]

MIDDLEWARE_CLASSES = [
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
   "django.middleware.csrf.CsrfResponseMiddleware",
   "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django_openid.consumer.SessionConsumer",
    "django.contrib.messages.middleware.MessageMiddleware",
    "pinax.apps.account.middleware.LocaleMiddleware",
    "pagination.middleware.PaginationMiddleware",
    "pinax.middleware.security.HideSensistiveFieldsMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    ]

                                                                                                                                      

ROOT_URLCONF = "foode.urls"

TEMPLATE_DIRS = [
    os.path.join(PROJECT_ROOT, "templates"),
]

TEMPLATE_CONTEXT_PROCESSORS = [
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    
    "staticfiles.context_processors.static",
    
    "pinax.core.context_processors.pinax_settings",
    
    "pinax.apps.account.context_processors.account",
     "messages.context_processors.inbox",
    "notification.context_processors.notification",
    "friends.context_processors.invitations",
    "announcements.context_processors.site_wide_announcements",
    "foode.context_processors.combined_inbox_count",
    'social_auth.context_processors.social_auth_by_name_backends',
    'social_auth.context_processors.social_auth_backends',
    'social_auth.context_processors.social_auth_by_type_backends',
]

COMBINED_INBOX_COUNT_SOURCES = [
    "messages.context_processors.inbox",
    "friends.context_processors.invitations",
    "notification.context_processors.notification",
]

INSTALLED_APPS = [
    # Django
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.humanize",
    
    "pinax.templatetags",
    
    # theme
    "pinax_theme_bootstrap",
    
    # external
    "notification", # must be first
    "staticfiles",
    "compressor",
    "debug_toolbar",
    "mailer",
    "django_openid",
    "timezones",
    "emailconfirmation",
    "announcements",
    "pagination",
    "idios",
    "metron",
    
    # Pinax
    "account",
    "pinax.apps.signup_codes",
    
    # project
    "about",
    "profiles",
    "avatar",
    "ajax_validation",
    "friends",
    "messages",
    "oembed",
    "threadedcomments",
    "threadedcomments_extras",
    "groups",
    "haystack",
     "voting",
     "socialstream",
    "socialJSON",
    "recipes",
    "restaurants",
    "photologue",
    "photos",
    "tagging",
    "tagging_ext",
    "flag",
    "djangoratings",
    "social_auth",
   "gunicorn",
]

FIXTURE_DIRS = [
    os.path.join(PROJECT_ROOT, "fixtures"),
]

MESSAGE_STORAGE = "django.contrib.messages.storage.session.SessionStorage"

EMAIL_BACKEND = "mailer.backend.DbBackend"

ABSOLUTE_URL_OVERRIDES = {
    "auth.user": lambda o: "/profiles/profile/%s/" % o.username,
}

AUTH_PROFILE_MODULE = "profiles.Profile"
NOTIFICATION_LANGUAGE_MODULE = "account.Account"

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': '/home/nayan/whoosh/index',
        'STORAGE': 'file',
        'POST_LIMIT': 128 * 1024 * 1024,
        'INCLUDE_SPELLING': True,
        'BATCH_SIZE': 100,
    },
}

#DEFAULT_MAX_COMMENT_LENGTH=160
#DEFAULT_MAX_COMMENT_DEPTH =2

TWITTER_CONSUMER_KEY         = 'KTWGOpzLfSHdpgV34cjMBQ'
TWITTER_CONSUMER_SECRET      = 'VJecqakLvb9Ff0LnSQDgQOZ5w3XWawOgi3yYLOmXw'
FACEBOOK_APP_ID              = '454208624593386'
FACEBOOK_API_SECRET          = '08640e93d589b45ff16683b4f36dcd82'
GOOGLE_OAUTH2_CLIENT_ID      = '379778216689.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET  = 'F09MlmmdLotuasJ1ctHu4h7q'

ACCOUNT_OPEN_SIGNUP = True
ACCOUNT_USE_OPENID = False
ACCOUNT_REQUIRED_EMAIL = False
ACCOUNT_EMAIL_VERIFICATION = False
ACCOUNT_EMAIL_AUTHENTICATION = False
ACCOUNT_UNIQUE_EMAIL = EMAIL_CONFIRMATION_UNIQUE_EMAIL = False
AVATAR_DEFAULT_URL = MEDIA_URL + "avatars/" + "tomato80.jpg" 
AVATAR_GRAVATAR_BACKUP = False

AUTHENTICATION_BACKENDS = [
    "pinax.apps.account.auth_backends.AuthenticationBackend",
    "django.contrib.auth.backends.ModelBackend",
    "social_auth.backends.twitter.TwitterBackend",
    "social_auth.backends.facebook.FacebookBackend",
    "social_auth.backends.google.GoogleOAuth2Backend",

]

LOGIN_URL = "/account/login/" # @@@ any way this can be a url name?
LOGIN_REDIRECT_URL = '/recipes/'

#LOGIN_REDIRECT_URLNAME = "home"
LOGOUT_REDIRECT_URLNAME = "home"
LOGIN_ERROR_URL    = '/login-error/'                                                                                                                                                         


SOCIAL_AUTH_ENABLED_BACKENDS = ('twitter','facebook') 
SOCIAL_AUTH_COMPLETE_URL_NAME  = 'socialauth_complete'
SOCIAL_AUTH_ASSOCIATE_URL_NAME = 'socialauth_associate_complete'
SOCIAL_AUTH_CREATE_USERS=True                                                                                                                                                      
SOCIAL_AUTH_FORCE_RANDOM_USERNAME=False                                                                                                                                                     
SOCIAL_AUTH_DEFAULT_USERNAME='socialauth_user'                                                                                                                                         
SOCIAL_AUTH_COMPLETE_URL_NAME='socialauth_complete'
SOCIAL_AUTH_ASSOCIATE_URL_NAME = 'socialauth_associate_complete'
FACEBOOK_EXTENDED_PERMISSIONS = ['email']
                                                                                                                        
SOCIAL_AUTH_ERROR_KEY             = 'socialauth_error'   
#SOCIAL_AUTH_LOGIN_REDIRECT_URL = 'home'
SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL = 'home'
SOCIAL_AUTH_DISCONNECT_REDIRECT_URL = 'home'

SOCIAL_AUTH_PIPELINE = (                                                                                                                                                                      
    'social_auth.backends.pipeline.social.social_auth_user',                                                                                                                                  
    'social_auth.backends.pipeline.associate.associate_by_email',                                                                                                                             
    'social_auth.backends.pipeline.misc.save_status_to_session',                                                                                                                              
    'social_auth.backends.pipeline.user.get_username',                                                                                                                                        
    'social_auth.backends.pipeline.user.create_user',                                                                                                                                         
    'social_auth.backends.pipeline.social.associate_user',                                                                                                                                    
    'social_auth.backends.pipeline.social.load_extra_data',                                                                                                                                   
    'social_auth.backends.pipeline.user.update_user_details'                                                                                                                                                                                                                                                                 
)        

EMAIL_CONFIRMATION_DAYS = 2
EMAIL_DEBUG = DEBUG

DEBUG_TOOLBAR_CONFIG = {
    "INTERCEPT_REDIRECTS": False,
}

# local_settings.py can be used to override environment-specific settings
# like database and email that differ between development and production.
try:
    from local_settings import *
except ImportError:
    pass
