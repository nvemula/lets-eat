# project.txt is a pip requirements file which describes the distributions
# required by your project to run.

--requirement=base.txt
gdata
vobject
cython
haystack whoosh index
ybrowserauth
lxml
create folders thumb_400_400,thumb_150_150,thumb_100_100 for restaurants app and recipes
install crab
install scikits-learn
use reverse instead of reverse_lazy

py-googlemaps

# Put project-specific requirements here.
# See http://pip-installer.org/requirement-format.html for more information.

