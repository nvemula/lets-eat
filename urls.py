from django.conf import settings
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()

from pinax.apps.account.openid_consumer import PinaxConsumer


handler500 = "pinax.views.server_error"


urlpatterns = patterns("",
  #  url(r"^$", direct_to_template, {
   #     "template": "landing.html",
    #}, name="home"),
    url(r"^$",'views.homeview',name="home"),
    url(r"^admin/invite_user/$", "pinax.apps.signup_codes.views.admin_invite_user", name="admin_invite_user"),
    url(r"^frienddecline/(?P<invitation_id>)/(?P<user_name>\w+)/$", "apps.friends.views.frienddecline", name="frienddecline"), 
    url(r"^friendaccept/(?P<invitation_id>)/(?P<user_name>\w+)/$", "apps.friends.views.friendaccept", name="friendaccept"), 
    url(r'', include('social_auth.urls')),

    url(r"^admin/", include(admin.site.urls)),
    url(r"^about/", include("apps.about.urls")),
    url(r"^account/", include("apps.account.urls")),
    url(r"^openid/", include(PinaxConsumer().urls)),
    url(r"^profiles/", include("profiles.urls")),
    url(r"^box/", include("notification.urls")),
    url(r"^announcements/", include("announcements.urls")),
    url(r"^displaypic/", include("apps.avatar.urls")),
    url(r"^friends/", include("friends.urls")),
    url(r"^messages/", include("messages.urls")),
    url(r"^comments/", include("threadedcomments.urls")),
    url(r"^i18n/", include("django.conf.urls.i18n")),
    url(r"^photos/", include("photos.urls")),
    url(r"^displaypic/", include("avatar.urls")),
    url(r"^flag/", include("flag.urls")),
    url(r"^recipes/",include("recipes.urls")),
    url(r"^restaurants/",include("restaurants.urls")),
    url(r"^socialstream/",include("socialstream.urls")),
    url(r"^search/", include("haystack.urls")),
)


#if settings.SERVE_MEDIA:
 #   urlpatterns += patterns("",
  #      url(r"", include("staticfiles.urls")),
   #     (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes':True}),
    #)
