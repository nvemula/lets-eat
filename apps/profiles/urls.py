from django.conf.urls.defaults import *

from views import *


urlpatterns = patterns("",
    url(r"^(?P<username>[\w\._-]+)/$", "profiles.views.oldprofile", name="profile_indetail"),
    url(r"^profile/edit/$", "profiles.views.profile_edit", name="editprofile"),
    url(r"^ajaxeditprofile/(?P<user_name>[\w\._-]+)/$", "profiles.views.profile_ajaxedit", name="ajaxeditprofile"),
     url(r"^username_autocomplete/$", "autocomplete_app.views.username_autocomplete_friends", name="profile_username_autocomplete"),
  # url(r"^profile/(?P<username>[\w\._-]+)/$", ProfileDetailView.as_view(), name="profile_detail"),
    #url(r"^profile/(?P<username>[\w\._-]+)/$", "profile", name="profile_detail"),
    #url(r"^(?P<profile_slug>[\w\._-]+)/profile/(?P<profile_pk>\d+)/$", ProfileDetailView.as_view(), name="profile_detail"),
   # url(r"^all/$", ProfileListView.as_view(all_profiles=True), 
         #  name="profile_list_all"),
    #url(r"", include("profiles.urls_base")),
   # url(r"^(?P<profile_slug>[\w\._-]+)/", include("profiles.urls_base")),
   # url(r"^edit/$", ProfileUpdateView.as_view(), name="profile_edit"),
)
