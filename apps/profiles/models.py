from django.db import models
from django.utils.translation import ugettext_lazy as _

from idios.models import ProfileBase


class Profile(ProfileBase):
      SEX_CHOICES = (
          ('Male','Male'),
          ('Female','Female'),
          ('Other','Other'),
        )
      FOODHABITS_CHOICES = (
           ('Vegetarian','Vegetarian'),
           ('Non-Vegetarian','Non-Vegetarian'),
      )
    
      name = models.CharField(_("Name"), max_length=35, null=True, blank=True)
      about = models.TextField(_("About"), null=True, blank=True)
      location = models.CharField(_("Location"), max_length=40, null=True, blank=True)
      website = models.URLField(_("Website or Blog"), null=True, blank=True, verify_exists=False)
      nickname = models.CharField(_("Nick name"),max_length=50,null=True,blank=True)
      sex = models.CharField(_("Sex"),max_length=50,blank=False,choices=SEX_CHOICES,default="Male")
      foodhabits = models.CharField(_("Food Habits"),
         max_length=256,
         blank=False,choices=FOODHABITS_CHOICES,default="Vegetarian"
      )
      favcuisines = models.CharField(_("Fav Cuisines"),
         max_length=1024,
         null=True,
         blank=True
      )
      resuser = models.BooleanField(_("resuser"),default=False)
     
      def get_absolute_url(self):
        pr = Profile.objects.filter(id=self.pk)
        return ("profile_indetail",pr)
      get_absolute_url = models.permalink(get_absolute_url)


   
