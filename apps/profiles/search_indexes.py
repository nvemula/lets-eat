import datetime
from haystack import indexes
from profiles.models import Profile

class ProfileIndex(indexes.SearchIndex,indexes.Indexable):
    text = indexes.CharField(use_template=True,document=True)
    name = indexes.CharField(model_attr='name',null=True)
    nickname = indexes.CharField(model_attr='nickname',null=True)
   
    def get_model(self):
        return Profile
    def index_queryset(self):
        "Used when the entire index for model is updated."
        return self.get_model().objects.all()


