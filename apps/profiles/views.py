from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import simplejson as json
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from profiles.forms import *
from utils import get_profile_model, get_profile_base
from friends.forms import InviteFriendForm
from friends.models import FriendshipInvitation, Friendship
try:
    from django.views.generic import ListView, DetailView, CreateView, UpdateView
except ImportError:
    try:
        from cbv import ListView, DetailView, CreateView, UpdateView
    except ImportError:
        raise ImportError(
            "It appears you are running a version of Django < "
            "1.3. To use idios with this version of Django, install "
            "django-cbv==0.1.5."
        )


if "notification" in settings.INSTALLED_APPS:
    from notification import models as notification
else:
    notification = None


def group_and_bridge(kwargs):
    """
    Given kwargs from the view (with view specific keys popped) pull out the
    bridge and fetch group from database.
    """
    
    bridge = kwargs.pop("bridge", None)
    
    if bridge:
        try:
            group = bridge.get_group(**kwargs)
        except ObjectDoesNotExist:
            raise Http404
    else:
        group = None
    
    return group, bridge


def group_context(group, bridge):
    # @@@ use bridge
    return {
        "group": group,
    }


class ProfileListView(ListView):
    """
    List all profiles of a given type (or the default type, if
    profile_slug is not given.)
    
    If all_profiles is set to True, all profiles are listed.
    """
    template_name = "idios/profiles.html"
    context_object_name = "profiles"
    all_profiles = False
    
    def get_model_class(self):
        
        # @@@ not group-aware (need to look at moving to profile model)
        profile_slug = self.kwargs.get("profile_slug", None)
        
        if self.all_profiles:
            profile_class = get_profile_base()
        else:
            profile_class = get_profile_model(profile_slug)
        
        if profile_class is None:
            raise Http404
        
        return profile_class
    
    def get_queryset(self):
        
        profiles = self.get_model_class().objects.select_related()
        profiles = profiles.order_by("-date_joined")
        
        search_terms = self.request.GET.get("search", "")
        order = self.request.GET.get("order", "date")
        
        if search_terms:
            profiles = profiles.filter(user__username__icontains=search_terms)
        if order == "date":
            profiles = profiles.order_by("-user__date_joined")
        elif order == "name":
            profiles = profiles.order_by("user__username")
        
        return profiles
    
    def get_context_data(self, **kwargs):
        
        group, bridge = group_and_bridge(self.kwargs)
        
        search_terms = self.request.GET.get("search", "")
        order = self.request.GET.get("order", "date")
        
        ctx = group_context(group, bridge)
        ctx.update({
            "order": order,
            "search_terms": search_terms,
        })
        ctx.update(
            super(ProfileListView, self).get_context_data(**kwargs)
        )
        
        return ctx


class ProfileDetailView(DetailView):
    
    template_name = "idios/profile.html"
    context_object_name = "profile"
    
    def get_object(self):
        
        username = self.kwargs.get("username")
        profile_class = get_profile_model(self.kwargs.get("profile_slug"))
        
        if profile_class is None:
            raise Http404
        
        if username:
            self.page_user = get_object_or_404(User, username=username)
            return get_object_or_404(profile_class, user=self.page_user)
        else:
            profile = get_object_or_404(
                profile_class, pk=self.kwargs.get("profile_pk")
            )
            self.page_user = profile.user
            return profile
    
    def get_context_data(self, **kwargs):
        
        base_profile_class = get_profile_base()
        profiles = base_profile_class.objects.filter(user=self.page_user)
        
        group, bridge = group_and_bridge(kwargs)
        is_me = self.request.user == self.page_user
        
        ctx = group_context(group, bridge)
        ctx.update({
            "is_me": is_me,
            "page_user": self.page_user,
            "profiles": profiles,
        })
        ctx.update(
            super(ProfileDetailView, self).get_context_data(**kwargs)
        )
        
        return ctx


class ProfileCreateView(CreateView):
    
    template_name = "templates/profiles/profile_create.html"
    template_name_ajax = "templates/profiles/profile_create_ajax.html"
    
    def get_template_names(self):
        
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]
    
    def get_form_class(self):
        
        if self.form_class:
            return self.form_class
        
        profile_class = get_profile_model(self.kwargs.get("profile_slug"))
        
        if profile_class is None:
            raise Http404
        
        return profile_class.get_form()
    
    def form_valid(self, form):
        
        profile = form.save(commit=False)
        profile.user = self.request.user
        profile.save()
        self.object = profile
        
        return HttpResponseRedirect(self.get_success_url())
    
    def get_context_data(self, **kwargs):
        
        group, bridge = group_and_bridge(self.kwargs)
        
        ctx = group_context(group, bridge)
        ctx.update(
            super(ProfileCreateView, self).get_context_data(**kwargs)
        )
        ctx["profile_form"] = ctx["form"]
        return ctx
    
    def get_success_url(self):
        
        if self.success_url:
            return self.success_url
        
        group, bridge = group_and_bridge(self.kwargs)
        return self.object.get_absolute_url(group=group)
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileCreateView, self).dispatch(*args, **kwargs)


class ProfileUpdateView(UpdateView):
    
    template_name = "templates/profiles/profile_edit.html"
    template_name_ajax = "templates/profiles/profile_edit_ajax.html"
    template_name_ajax_success = "templates/profiles/profile_edit_ajax_success.html"
    context_object_name = "profile"
    
    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        else:
            return [self.template_name]
    
    def get_form_class(self):
        if self.form_class:
            return self.form_class
        
        profile_class = get_profile_model(self.kwargs.get("profile_slug"))
        
        if profile_class is None:
            raise Http404
        
        return profile_class.get_form()
    
    def get_object(self, queryset=None):
        profile_class = get_profile_model(self.kwargs.get("profile_slug"))
        if profile_class is None:
            raise Http404
        
        profile = profile_class.objects.get(user=self.request.user)
        return profile
    
    def get_context_data(self, **kwargs):
        group, bridge = group_and_bridge(self.kwargs)
        ctx = group_context(group, bridge)
        ctx.update(
            super(ProfileUpdateView, self).get_context_data(**kwargs)
        )
        ctx["profile_form"] = ctx["form"]
        return ctx
    
    def form_valid(self, form):
        self.object = form.save()
        if self.request.is_ajax():
            data = {
                "status": "success",
                "location": self.object.get_absolute_url(),
                "html": render_to_string(self.template_name_ajax_success),
            }
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            return HttpResponseRedirect(self.get_success_url())
    
    def form_invalid(self, form):
        if self.request.is_ajax():
            ctx = RequestContext(self.request, self.get_context_data(form=form))
            data = {
                "status": "failed",
                "html": render_to_string(self.template_name_ajax, ctx),
            }
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            return self.render_to_response(self.get_context_data(form=form))
    
    def get_success_url(self):
        
        if self.success_url:
            return self.success_url
        
        group, bridge = group_and_bridge(self.kwargs)
        return self.object.get_absolute_url(group=group)
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        try:
            return super(ProfileUpdateView, self).dispatch(*args, **kwargs)
        except:
            import sys, traceback
            traceback.print_exc()

@login_required
def oldprofile(request, username, template_name="profiles/newprofile.html", extra_context=None):
    
    messages=[]
    is_following = False
    if extra_context is None:
        extra_context = {}
    
    other_user = get_object_or_404(User, username=username)
   
    if request.user.is_authenticated():
        is_friend = Friendship.objects.are_friends(request.user, other_user)
        other_friends = Friendship.objects.friends_for_user(other_user)
        if request.user == other_user:
            is_me = True
        else:
            is_me = False
    else:
        other_friends = []
        is_friend = False
        is_me = False
        is_following = False
    
    if is_friend:
        invite_form = None
        previous_invitations_to = None
        previous_invitations_from = None
        if request.method == "POST":
            if request.POST.get("action") == "remove": # @@@ perhaps the form should just post to friends and be redirected here
                Friendship.objects.remove(request.user, other_user)
               # messages.add_message(request, messages.SUCCESS,
                #    ugettext("You have removed %(from_user)s from friends") % {
                 #       "from_user": other_user
                  #  }
                #)
                is_friend = False
                invite_form = InviteFriendForm(request.user, {
                    "to_user": username,
                    "message": ugettext("Let's be friends!"),
                })
    
    else:
        if request.user.is_authenticated() and request.method == "POST":
            if request.POST.get("action") == "invite": # @@@ perhaps the form should just post to friends and be redirected here
                invite_form = InviteFriendForm(request.user, request.POST)
                if invite_form.is_valid():
                    invite_form.save()
            else:
                invite_form = InviteFriendForm(request.user, {
                    "to_user": username,
                    "message": ugettext("Let's be friends!"),
                })
                invitation_id = request.POST.get("invitation", None)
                if request.POST.get("action") == "accept": # @@@ perhaps the form should just post to friends and be redirected here
                    try:
                        invitation = FriendshipInvitation.objects.get(id=invitation_id)
                        if invitation.to_user == request.user:
                            invitation.accept()
                          #  messages.add_message(request, messages.SUCCESS,
                           #     ugettext("You have accepted the friendship request from %(from_user)s") % {
                            #        "from_user": invitation.from_user
                             #   }
                            #)
                            is_friend = True
                            other_friends = Friendship.objects.friends_for_user(other_user)
                    except FriendshipInvitation.DoesNotExist:
                        pass
                elif request.POST.get("action") == "decline": # @@@ perhaps the form should just post to friends and be redirected here
                    try:
                        invitation = FriendshipInvitation.objects.get(id=invitation_id)
                        if invitation.to_user == request.user:
                            invitation.decline()
                            #messages.add_message(request, messages.SUCCESS,
                             #   ugettext("You have declined the friendship request from %(from_user)s") % {
                              #      "from_user": invitation.from_user
                               # }
                            #)
                            other_friends = Friendship.objects.friends_for_user(other_user)
                    except FriendshipInvitation.DoesNotExist:
                        pass
        else:
            invite_form = InviteFriendForm(request.user, {
                "to_user": username,
                "message": ugettext("Let's be friends!"),
            })
    
    previous_invitations_to = FriendshipInvitation.objects.invitations(to_user=other_user, from_user=request.user)
    previous_invitations_from = FriendshipInvitation.objects.invitations(to_user=request.user, from_user=other_user)
    
    return render_to_response(template_name, dict({
        "is_me": is_me,
        "is_friend": is_friend,
        "is_following": is_following,
        "other_user": other_user,
        "other_friends": other_friends,
        "invite_form": invite_form,
        "previous_invitations_to": previous_invitations_to,
        "previous_invitations_from": previous_invitations_from,
    }, **extra_context), context_instance=RequestContext(request))

@login_required
def profile_edit(request, form_class=ProfileForm, **kwargs):
    
    template_name = kwargs.get("template_name", "profiles/profile_edit.html")
    
   
     
    
    profile = request.user.get_profile()
    
    if request.method == "POST":
        profile_form = form_class(request.POST, instance=profile)
        if profile_form.is_valid():
            profile = profile_form.save(commit=False)
            profile.user = request.user
            profile.save()
            return HttpResponseRedirect(reverse("profile_indetail", args=[request.user.username]))
    if request.is_ajax():
        profile_form = form_class(instance=profile)
    else:
        profile_form = form_class(instance=profile)
    
    return render_to_response(template_name, {
        "profile": profile,
        "profile_form": profile_form,
    }, context_instance=RequestContext(request))

def profile_ajaxedit(request,user_name,**kwargs):
    template_name = kwargs.get("template_name", "profiles/profile_edit.html")
    user=User.objects.get(username=user_name)
    profile = user.get_profile()
    if request.is_ajax and request.method == "POST":
        profile_form = ProfileForm(request.POST,instance=profile)
        if profile_form.is_valid():
           profile = profile_form.save(commit=False)
           profile.user = user
           profile.save()
           return HttpResponse("Successfully updated")
    else:
        return HttpResponse("Failed to update")

