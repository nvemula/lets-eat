#django
from django.shortcuts import render_to_response, get_object_or_404,HttpResponse
from django.http import HttpResponseRedirect, Http404
from django.template import RequestContext
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from datetime import datetime
from django.utils.simplejson import dumps


#pinax 
from friends.models import *

from socialJSON.myJSON import *
#socialstream
from socialstream.models import *

@login_required
def userstream(request,other_user):
  """
   A  user stream shows the activities of a particular user
  """
  user = User.objects.filter(username=other_user)
  actions = Action.objects.filter(actor=user)
  return render_to_response("socialstream/stream.html",{"actions":actions}, context_instance=RequestContext(request))

@login_required
def stream(request):
  """
   Stream shows the activities of the friends of the user and the user
  """
  user = User.objects.get(username=request.user)
  friends = friend_set_for(user)
  friends = list(friends)
  friends.append(user)
  actions = Action.objects.filter(actor__in=friends,privacy="")
  return render_to_response("socialstream/stream.html",{"actions":actions}, context_instance=RequestContext(request))

def globalstream(request):
  """
   Stream that shows the activities of all the users 
  """
  actions = Action.objects.all()
  return render_to_response("socialstream/stream.html",{"actions":actions}, context_instance=RequestContext(request))

def usstream(request,other_user):
  """
   A  user stream shows the activities of a particular user
  """
  user = User.objects.filter(username=other_user)
  actions = Action.objects.filter(actor=user)
  return activities_to_json(actions)
  
def homestream(request,user_name):
  """
   Stream shows the activities of the friends of the user and the user
  """
  user = User.objects.get(username=user_name)
  friends = friend_set_for(user)
  friends = list(friends)
  friends.append(user)
  actions = Action.objects.filter(actor__in=friends,privacy="")
  return activities_to_json(actions)
