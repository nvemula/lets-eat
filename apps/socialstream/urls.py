from django.conf.urls.defaults import *

urlpatterns = patterns('',
 
   url(r'^(?P<other_user>[\w\._-]+)/new/$','socialstream.views.userstream',name='userstream'),
   url(r'^stream/$','socialstream.views.stream',name='stream'),
   url(r'^globalstream/$','socialstream.views.globalstream',name='globalstream'),
   url(r'^(?P<other_user>[\w\._-]+)/ajax/$','socialstream.views.usstream',name='usstream'),
   url(r'^homestream/(?P<user_name>[\w\._-]+)/$','socialstream.views.homestream',name='homestream'),
)
