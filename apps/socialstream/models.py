from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

class Action(models.Model):
   """
    An action can be any of the following
    User adding his fav recipes,restaurants to his profile
    User editing his recipes,restaurant
    User checkins
   """
    
   actor = models.ForeignKey(User,related_name="actor")
   verb = models.CharField(max_length=255)
   action_object = models.CharField(max_length=2)
   objectname = models.CharField(max_length=255)
   objectid = models.CharField(max_length=255)
   timestamp = models.DateTimeField(default=datetime.now)
   privacy = models.CharField(max_length=255,blank=True)
   
   def timesince(self, now=None):
        """
        Shortcut for the ``django.utils.timesince.timesince`` function of the current timestamp
        """
        from django.utils.timesince import timesince as timesince_
        return timesince_(self.timestamp, now)
   
   class Meta:
        ordering = ('-timestamp',)
 
   
