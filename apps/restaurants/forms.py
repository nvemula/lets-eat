#from django
from django import forms
from django.utils.translation import ugettext_lazy as _

#from restaurants
from restaurants.models import Restaurant

class RestaurantForm(forms.ModelForm):
    """
    Restaurant Form: form associated to the Restaurant model
    """
    resart=forms.ImageField(label="Res Art")  

    def __init__(self, *args, **kwargs):
        super(RestaurantForm, self).__init__(*args, **kwargs)
        self.is_update = False
    
    def clean(self):
        """ Do validation stuff. """
        # name is mandatory
        if 'name' not in self.cleaned_data:
            return
        # if a restaurant with that title already exists...
        if not self.is_update:
            if Restaurant.objects.filter(name=self.cleaned_data['name']).count() > 0:
                raise forms.ValidationError(_("There is already this restaurant in the database"))
        return self.cleaned_data
    
    class Meta:
        model = Restaurant
        fields = ('resart','name','alsoknownas','category','serves','description','cuisines','address','city','state','country','zipcode','phone','email','website',
                  'hours','pricerange','creditcards','ac','wifi','ambiance','music','parking','smoking')

