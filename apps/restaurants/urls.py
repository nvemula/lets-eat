#from django
from django.conf.urls.defaults import *

# from restaurants
from restaurants.models import Restaurant

urlpatterns = patterns('',
        url(r'^$', 'restaurants.views.restaurants', name="restaurants"),
        url(r'^(\d+)/restaurant/$', 'restaurants.views.restaurant', name="describe_restaurant"),
        url(r'^searchrestaurants/$', 'restaurants.views.searchrestaurants', name="searchrestaurants"),

        url(r'^myrestaurants/(?P<user_name>[\w\._-]+)/$', 'restaurants.views.myrestaurants', name="myrestaurants"),
        url(r'^(\d+)/menulist/$', 'restaurants.views.menulist', name="menulist"),
          url(r'^(\d+)/editmenulist/$', 'restaurants.views.editmenulist', name="editmenulist"),
        url(r'^(\d+)/menuitem/$', 'restaurants.views.menuitem', name="menuitem"),
          url(r'^(\d+)/editmenuitem/$', 'restaurants.views.editmenuitem', name="editmenuitem"),
        url(r'^(\d+)/removemenuitem/$', 'restaurants.views.removemenuitem', name="removemenuitem"),   
        url(r'^ratemenuitem/(?P<menuitem_id>\w+)/(?P<user_name>[\w\._-]+)/(?P<rating>\w+)/$', 'restaurants.views.ratemenuitem', name="ratemenuitem"),   
        url(r'^(\d+)/removemenulist/$', 'restaurants.views.removemenulist', name="removemenulist"),   
         url(r'^(\d+)/editmenulistview/$', 'restaurants.views.editmenulistview', name="editmenulistview"),
         url(r'^menulistview/(?P<restaurant_id>\w+)/(?P<user_name>[\w\._-]+)/$', 'restaurants.views.menulistview', name="menulistview"),
         url(r'^userratingmenuitem/(?P<user_name>[\w\._-]+)/(?P<restaurant_id>\w+)/$', 'restaurants.views.userratingmenuitem', name="userratingmenuitem"),

    
        #CRUD urls
        url(r'^add/$', 'restaurants.views.add_restaurant', name="add_restaurant"),
        url(r'^(\d+)/edit/$', 'restaurants.views.edit_restaurant', name="edit_restaurant"),
        url(r'^(\d+)/delete/$', 'restaurants.views.delete_restaurant', name="delete_restaurant"),
        url(r'^favourite/(?P<restaurant_id>\w+)/(?P<user_name>[\w\._-]+)/$', 'restaurants.views.favourite_restaurant', name="favourite_restaurant"),
        url(r'^favremove/(?P<restaurant_id>\w+)/(?P<user_name>[\w\._-]+)/$','restaurants.views.favremove_restaurant', name="favremove_restaurant"),

        url(r'^restaurant_detail/(?P<other_res>\w+)/$', 'restaurants.views.restaurant_detail', name="restaurant_detail"),
        url(r'^restaurantshuffle/$', 'restaurants.views.restaurantshuffle', name="restaurantshuffle"),
        url(r'^trendingrestaurants/$', 'restaurants.views.trendingrestaurants', name="trendingrestaurants"),
        url(r'^(\d+)/recipesync/$', 'restaurants.views.recipesync', name="recipesync"),
        url(r'^(\d+)/resetsync/$', 'restaurants.views.resetsync', name="resetsync"),
)

