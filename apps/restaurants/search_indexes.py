import datetime
from haystack import indexes
from restaurants.models import Restaurant

class RestaurantIndex(indexes.SearchIndex,indexes.Indexable):
    text = indexes.CharField(use_template=True,document=True)
    name = indexes.CharField(model_attr='name')
    city = indexes.CharField(model_attr='city')
   
    def get_model(self):
        return Restaurant
    def index_queryset(self):
        "Used when the entire index for model is updated."
        return self.get_model().objects.filter(added__lte=datetime.datetime.now())


