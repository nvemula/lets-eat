from django.contrib import admin
from restaurants.models import Restaurant

class RestaurantAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'city', 'added', 'adder', 'resart')

admin.site.register(Restaurant, RestaurantAdmin)


