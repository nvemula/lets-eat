#from django
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect,HttpResponse,HttpRequest
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.db.models import Q

#from restaurants
from restaurants.models import *
from restaurants.forms import RestaurantForm

#from recipes
from recipes.models import Recipe

#from pinax
from profiles.models import Profile

#from djangoratings
from djangoratings.views import *

from socialJSON.myJSON import *
from socialstream.models import *
from django.utils.simplejson import *

def restaurants(request):
    """ Return the all restaurants list, ordered by added date. """
    restaurant = Restaurant.objects.all().order_by("-added")
    return render_to_response("restaurants/restaurants.html", {
        "restaurants": restaurants,
        "list": 'all',
    }, context_instance=RequestContext(request))
    
def restaurant(request, restaurant_id):
    """ Return a restaurant given its id. """
    restaurant = Restaurant.objects.get(id=restaurant_id)
    if request.user.is_authenticated():
       isyours = False
       favbutton = False
       ratingform = False
       chainbutton = False
       reuser = False
       resrating = RestaurantRating.objects.filter(resname=restaurant,rateduser=request.user) 
       if not resrating:
          ratingform = True
       resfav = RestaurantFav.objects.filter(resname=restaurant,favouritedusers=request.user)
       if not resfav:
          favbutton = True
       if request.user == restaurant.adder:
          isyours = True
       rus = Profile.objects.get(user=request.user)
       if rus.resuser:
          reuser = True
          res1=restaurant
          res2 = Restaurant.objects.get(adder=request.user)
          rchain = RestaurantChain.objects.filter(Q(fromres=res1,tores=res2) | Q(fromres=res2,tores=res1))
          if not rchain:
             chainbutton = True
          else:
             chainbutton = False
       return render_to_response("restaurants/restaurant.html", {
        "restaurant": restaurant,
        "isyours": isyours,
        "favbutton":favbutton, 
        "ratingform":ratingform,
        "chainbutton":chainbutton, 
        "reuser":reuser, 
       }, context_instance=RequestContext(request))
    else:
          favbutton = True
          isyours = False
          ratingform = False 
          menubutton = "Offline"
          chainbutton = "Offline"
          reuser = False
          return render_to_response("restaurants/restaurant.html",{
          "restaurant":restaurant,
          "isyours": isyours,
          "favbutton":favbutton, 
          "ratingform":ratingform,
          "menubutton":menubutton,
          "chainbutton":chainbutton,
          "reuser":reuser,
          }, context_instance=RequestContext(request))  

@login_required
def your_restaurants(request):
    """ Return the logged user restaurants list. """
    yourrestaurants = Restaurant.objects.filter(adder=request.user).order_by("-added")
    return render_to_response("restaurants/your_restaurants.html", {
        "restaurants": yourrestaurants,
        "list": 'yours',
    }, context_instance=RequestContext(request))   

@login_required
def add_restaurant(request): 
    #""" Add a restaurant to the database. """
    # POST request
    if request.method == "POST":
       restaurant_form = RestaurantForm(request.POST, request.FILES)
       if restaurant_form.is_valid():
        # from ipdb import set_trace; set_trace()
          new_restaurant = restaurant_form.save(commit=False)
          new_restaurant.adder = request.user
          new_restaurant.save()
          action = Action(actor=request.user,verb="added",action_object=2,objectid=new_restaurant.id,objectname=new_restaurant.name)
          action.save()
          request.user.message_set.create(message=_("You have saved restaurant '%(name)s'") %  {'name': new_restaurant.name})
          return HttpResponseRedirect(reverse("restaurants.views.your_restaurants"))
         
    # GET request
    else:
           restaurant_form = RestaurantForm()
           return render_to_response("restaurants/add.html", {
               "restaurant_form": restaurant_form,
               }, context_instance=RequestContext(request))
    # generic case
    return render_to_response("restaurants/add.html", {
       "restaurant_form": restaurant_form,
       }, context_instance=RequestContext(request)) 
 

@login_required
def edit_restaurant(request, restaurant_id):
    """ Update a restaurant given its id. """
    restaurant = Restaurant.objects.get(id=restaurant_id)
    if request.method == "POST":
       restaurant_form = RestaurantForm(request.POST, request.FILES, instance=restaurant)
       restaurant_form.is_update = True
       if request.user == restaurant.adder:
            #from ipdb import set_trace; set_trace()
          if restaurant_form.is_valid():
             restaurant_form.save()
             action = Action(actor=request.user,verb="edited",action_object=2,objectid=restaurant_id,objectname=restaurant.name)
             action.save()
             request.user.message_set.create(message=_("You have edited restaurant '%(name)s'") %  {'name': restaurant.name})
             return HttpResponseRedirect(reverse("restaurants.views.your_restaurants"))          
    else:
        restaurant_form = RestaurantForm(instance=restaurant)
        return render_to_response("restaurants/edit.html", {
            "restaurant_form": restaurant_form,
            "restaurant": restaurant,
            }, context_instance=RequestContext(request))  
              
@login_required
def delete_restaurant(request, restaurant_id):
    """ Delete a restaurant given its id. """
    restaurant = get_object_or_404(Restaurant, id=restaurant_id)
    if request.user == restaurant.adder:
        restaurantfavs = []
        restaurantfavs = RestaurantFav.objects.filter(resname=restaurant)
        for resfav in restaurantfavs:
            resfav.delete()
        restaurant.delete()
        rus = Profile.objects.get(user=request.user)
        rus.resuser = False
        request.user.message_set.create(message="Restaurant Deleted")
        
    return HttpResponseRedirect(reverse("restaurants.views.your_restaurants"))

@login_required
def favourite_restaurant(request,restaurant_id):
     """Adds a restaurant to the user's favorite restaurants given its id."""
     if request.method =="POST":
        restaurant = get_object_or_404(Restaurant,id=restaurant_id)
        resfav = RestaurantFav.objects.filter(resname=restaurant,favouritedusers=request.user)
        if not resfav:
           resfav = RestaurantFav(resname=restaurant)
           resfav.save()
           resfav.favouritedusers.add(request.user)         
           action = Action(actor=request.user,verb="favorited",action_object=2,objectid=restaurant_id,objectname=restaurant.name)
           action.save()
        return HttpResponseRedirect(reverse('describe_restaurant',args=[restaurant_id]))

@login_required
def favremove_restaurant(request,restaurant_id):
     """ Removes a restaurant from users favourite restaurants given its id."""
     if request.method =="POST":
        restaurant = get_object_or_404(Restaurant,id=restaurant_id)
        resfav = RestaurantFav.objects.get(resname=restaurant,favouritedusers=request.user)
        resfav.delete()
     return HttpResponseRedirect(reverse('describe_restaurant',args=[restaurant_id]))


@login_required
def rate_restaurant(request,restaurant_id):#"""Assigns a rating to a restaurant when a user rates """
       if request.method =="POST":
          restaurant = get_object_or_404(Restaurant,id=restaurant_id)
          resrate = RestaurantRating(resname = restaurant)
          resrate.save()
          resrate.resrating = request.POST["resrating"]
          resrate.rateduser.add(request.user)
          resrate.save()
       return HttpResponseRedirect(reverse('describe_restaurant',args=[restaurant_id]))

@login_required
def addtomenu_restaurant(request,recipe_id):
     """Adds a recipe to the menu of a restaurant given its id when the admin of a restaurant adds it."""
     rus = Profile.objects.get(user=request.user)
     if rus.resuser:
        if request.method =="POST":
           restaurant = get_object_or_404(Restaurant,adder=request.user)
           recipe = get_object_or_404(Recipe,id=recipe_id)
           resmenu = RestaurantMenu.objects.filter(resnames=restaurant,menuitem=recipe)
           action = Action(actor=request.user,verb="edited the menu of",action_object=2,objectid=restaurant.id,objectname=restaurant.name)
           action.save()
        if not resmenu:
           resmenu = RestaurantMenu(menuitem=recipe)
           resmenu.save()
           resmenu.resnames.add(restaurant)         
           action = Action(actor=request.user,verb="edited the menu of ",action_object=2,objectid=restaurant.id,objectname=restaurant.name)
           action.save()
     return HttpResponseRedirect(reverse('describe_recipe',args=[recipe_id]))

@login_required
def removefrommenu_restaurant(request,recipe_id):
     """ Removes a recipe from the restaurant's menu given its id."""
     rus = Profile.objects.get(user=request.user)
     if rus.resuser: 
        if request.method =="POST":
           restaurant = get_object_or_404(Restaurant,adder=request.user)
           if restaurant:
              resmenu = RestaurantMenu.objects.get(resnames=restaurant,menuitem=recipe_id)
              resmenu.delete()
              action = Action(actor=request.user,verb="edited the menu of ",action_object=2,objectid=restaurant.id,objectname=restaurant.name)
              action.save()
     return HttpResponseRedirect(reverse('describe_recipe',args=[recipe_id]))

@login_required
def addtochain(request,restaurant_id):
    """ Adds a restaurant to the chain of restaurants. """
    if request.method =="POST":
       res1 = Restaurant.objects.get(adder=request.user)
       res2 = Restaurant.objects.get(id=restaurant_id)
       rchain = RestaurantChain.objects.filter(Q(fromres=res1,tores=res2) | Q(fromres=res2,tores=res1))
       if not rchain:
          rcha = RestaurantChain(fromres=res1)
          rcha.status = False
          rcha.tores = res2 
          rcha.save()
          action = Action(actor=request.user,verb="updated the chain of",action_object=2,objectid=restaurant_id,objectname=restaurant.name)
          action.save()
       else:
          if not rchain[0].status:
             rchain[0].status = True
    return HttpResponseRedirect(reverse('describe_restaurant',args=[restaurant_id]))
    
@login_required
def removefromchain(request,restaurant_id):
    """Removes a restaurant from the chain of restaurants."""
    res1 = Restaurant.objects.get(adder=request.user)
    res2 = Restaurant.objects.get(id=restaurant_id)   
    rchain = RestaurantChain.objects.filter(Q(fromres=res1,tores=res2) | Q(fromres=res2,tores=res1))
    if rchain:    
       rchain[0].delete()
       action = Action(actor=request.user,verb="updated the chain of ",action_object=2,objectid=restaurant_id,objectname=restaurant.name)
       action.save()
    return HttpResponseRedirect(reverse('describe_restaurant',args=[restaurant_id]))

def restaurant_detail(request,other_res):
    restaurant = get_object_or_404(Restaurant,id=other_res)
    resmenu = RestaurantMenu.objects.filter(resnames=restaurant)
    recipes = []
    for recipe in resmenu:
        recipes.append(recipe.menuitem)
    reschain = RestaurantChain.objects.filter(Q(fromres=restaurant)|Q(tores=restaurant))
    restaurants=[] 
    for tmp in reschain:
        if tmp.fromres == restaurant:   
           restaurants.append(tmp.tores)
        if tmp.tores == restaurant:
           restaurants.append(tmp.fromres)
    tmprecipes = recipes_to_jsonready(recipes)
    tmpres = restaurants_to_jsonready(restaurants)
    res_data = {}
    res_data["menurecipes"]=tmprecipes
    res_data["chainrestaurants"]=tmpres
    return JSONResponse(res_data);

def menulist(request,restaurant_id):
    if request.method =="POST" and request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id)   
       datatmp = request.POST
       data = datatmp.values()
       for temp in data:
           menul = MenuList(res=restaurant,listname=temp)
           menul.save()
       msg = "Successfully added to the menu."          
       

    else:
        msg = "Failed to add to the menu.Try again"

    return HttpResponse(msg)    

def editmenulist(request,restaurant_id):
    if request.method =="POST" and request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id)   
       datatmp = request.POST
       data = datatmp.keys()
       for temp in data:
           menul = MenuList.objects.get(res=restaurant,id=temp)
           menul.listname=datatmp[temp]
           menul.save()
       msg = "Successfully updated the menu."          
       

    else:
        msg = "Failed to save the menu"

    return HttpResponse(msg)   

def removemenulist(request,menulist_id):
    if request.is_ajax:
       menul = MenuList.objects.get(id=menulist_id)
       items = MenuItem.objects.filter(menulist=menul)
       for item in items:
           item.delete()
       menul.delete()
       msg = "Successfully removed from the menu."          
    else:
        msg = "Failed to remove from the menu"

    return HttpResponse(msg)   

def menulistview(request,restaurant_id,user_name):
    restaurant = Restaurant.objects.get(id=restaurant_id)
    menulists = MenuList.objects.filter(res=restaurant)
    user = User.objects.filter(username=user_name)
    tmplists = menulists_to_jsonready(menulists,user)
    res_data={}
    res_data["menulists"]=tmplists
    return JSONResponse(res_data);



def menuitem(request,restaurant_id):
    if request.method =="POST" and request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id)   
       datatmp = request.POST
       keys = datatmp.keys()
       for key in keys:
           tt = datatmp[key]
           b = key.rpartition('z')[0][11:]
           listmenu = MenuList.objects.get(id=b)
           menuitem = MenuItem(resname=restaurant,menulist=listmenu,menuitem=tt)
           menuitem.save()
       msg = "Successfully added to the menu."          
    else:
        msg = "Failed to add to the menu.Try again"

    return HttpResponse(str(msg))        

def editmenuitem(request,restaurant_id):
    if request.method =="POST" and request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id)   
       datatmp = request.POST
       data = datatmp.keys()
       for temp in data:
           menui = MenuItem.objects.get(resname=restaurant,id=temp)
           menui.menuitem=datatmp[temp]
           menui.save()
       msg = "Successfully updated the menu."          
       

    else:
        msg = "Failed to save the menu"

    return HttpResponse(msg)   

def removemenuitem(request,menuitem_id):
    if request.is_ajax:
           menuitem = MenuItem.objects.get(id=menuitem_id)
           menuitem.delete()
           msg = "Successfully deleted from the list and menu."          
    else:
        msg = "Failed to remove from the menu"

    return HttpResponse(str(msg))        

def ratemenuitem(request,menuitem_id,user_name,rating):
    if request.is_ajax:
         itemid = menuitem_id[5:] 
         menuitem = MenuItem.objects.get(id=itemid)
         user = User.objects.get(username=user_name)
         menuitem.rating.add(score=rating, user=user,ip_address=request.META['REMOTE_ADDR'])
         msg = "Successfully rated the menuitem"          
    else:
        msg = "Failed to rate the menuitem"

    return HttpResponse(str(msg))        

def userratingmenuitem(request,restaurant_id,user_name):
    restaurant = Restaurant.objects.get(id=restaurant_id)  
    user = User.objects.get(username=user_name)
    menui = MenuItem.objects.filter(resname=restaurant)
    tmpitems = menuitemsrating_to_jsonready(menui,user)
    res_data={}
    res_data["userratings"]=tmpitems    
    return JSONResponse(res_data);  

      

