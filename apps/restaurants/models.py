# python
import os
from os import path
from datetime import datetime

# django imports
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

#from recipe
from recipes.models import Recipe

#from djangoratings
from djangoratings.fields import RatingField


class Restaurant(models.Model):
    """
    Restaurant Model: resart,name,category,description,address,city,state,country,zipcode,phone,website,hours,creditcards, adder,added,
      """   
    CATEGORY_CHOICES = (
         ('Buffet','Buffet'),
         ('Cafe','Cafe'),
         ('Coffeehouse','Coffee / Tea House'),
         ('Family','Family Style'),
         ('Fastfood','Fast Food'),
         ('General','General Restaurant'),
         ('Hotel','Hotel'),
         ('Pub','Pub / Bar'),
         ('Other','Other'),
     )
    CREDITCARDS_CHOICES = (
         ('Accepted','Accepted'),
         ('Not Accepted','Not Accepted'),
     )
    AC_CHOICES = (
         ('Yes','Yes'),
         ('No','No'),
     )
    WIFI_CHOICES = (
         ('Free Wi-Fi','Free Wi-Fi'),
         ('Charged Separately','Charged Separately'),
         ('Not Available','Not Available'),
     )
    PRICERANGE_CHOICES = (
         ('Cheap','Cheap'),
         ('Moderate','Moderate'),
         ('High','High'),
         ('Expensive','Expensive'),
     )
    MUSIC_CHOICES = (
         ('is Played','is Played'),
         ('Not Available','Not Available'),
    )
    SERVES_CHOICES = (
         ('Pure Veg','Pure Veg'),
         ('Non-Veg & Veg','Non-Veg & Veg'),
         ('Non-Veg','Non-Veg'),
    )
    SMOKING_CHOICES = (
         ('Strictly Prohibited','Strictly Prohibited'),
         ('Allowed','Allowed'),
    )
    resart = models.ImageField(_('Restaurant Art'),upload_to="restaurants", blank=True, null=True) 
    name = models.CharField(_('Name'), max_length=255,null=False)
    alsoknownas = models.CharField(_('Also Known As'), max_length=255)
    serves = models.CharField(_('Serves'), max_length=255,null=False,choices=SERVES_CHOICES,default="Pure Veg")
    category = models.CharField(_('Category'), max_length=255,null=False,choices=CATEGORY_CHOICES,default="Hotel")
    description = models.TextField(_('Description'),blank=True,null=True)
    cuisines =  models.TextField(_('Cuisines'),blank=True,null=False)
    address =  models.TextField(_('Address'),blank=True,null=False)
    city = models.CharField(_('City'),max_length=255,null=False)
    state = models.CharField(_('State'),max_length=255,null=False)
    country = models.CharField(_('Country'),max_length=255,null=False)
    zipcode = models.CharField(_('PIN / Zip Code'),max_length=25,blank=False,null=False)
    phone = models.CharField(_('Phone'),max_length=255,null=False)
    ac = models.CharField(_('Air Conditioned'),max_length=50,blank=False,null=False,choices=AC_CHOICES,default="No")
    wifi = models.CharField(_('Wi-Fi'),max_length=50,blank=False,null=False,choices=WIFI_CHOICES,default="Not Available")
    music = models.CharField(_('Music'),max_length=50,blank=False,null=False,choices=MUSIC_CHOICES,default="Not Available")
    email = models.EmailField(_('Email'),
        null = True,
        blank = True,
    )
    website = models.URLField(_('Website'),
        null = True,
        blank = True,
        verify_exists = False
    )
    hours = models.TextField(_('Working Hours'),blank=False,null=False)
    parking = models.TextField(_('Parking'),blank=True,null=True)
    smoking = models.CharField(_('Smoking'),max_length=50,blank=True,null=True,choices=SMOKING_CHOICES,default="Strictly Prohibited")
    pricerange = models.CharField(_('Price Range'),max_length=50,blank=False,null=False,choices=PRICERANGE_CHOICES,default="Not Accepted")
    creditcards = models.CharField(_('Credit Cards'),max_length=50,blank=False,null=False,choices=CREDITCARDS_CHOICES,default="Not Accepted")
    ambiance =  models.TextField(_('Ambiance'),blank=True,null=False)
    rating = RatingField(range=5,can_change_vote = True) # 5 possible rating values, 1-5
    adder = models.ForeignKey(User, related_name="added_restaurants", verbose_name=_('adder'))
    added = models.DateTimeField(_('added'), default=datetime.now)

    def get_absolute_url(self):
        return ("describe_restaurant", [self.pk])
    get_absolute_url = models.permalink(get_absolute_url)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ('-added', )
        
    def _get_thumb_url(self, folder, size):
        """ get a thumbnail giver a folder and a size. """
        if not self.resart:
            return '#'       
        upload_to = path.dirname(self.resart.path)
        tiny = path.join(upload_to, folder, path.basename(self.resart.path))
        tiny = path.normpath(tiny)
        if not path.exists(tiny):
            import Image
            im = Image.open(self.resart.path)
            im.thumbnail(size, Image.ANTIALIAS)
            im.save(tiny, 'JPEG')  
        return path.join(path.dirname(self.resart.url), folder, path.basename(self.resart.path)).replace('\\', '/')

    def get_thumb_url(self):
        return self._get_thumb_url('thumb_400_400', (400,400))

    def get_thumbmedium_url(self):
        return self._get_thumb_url('thumb_100_100', (100,100))
    
    def get_thumbsmall_url(self):
        return self._get_thumb_url('thumb_56_56', (56,56))

    def thumb(self):
        """ Get thumb <a>. """
        link = self.get_thumb_url()
        if link is None:
            return '<a href="#" target="_blank">NO IMAGE</a>'
        else:
            return '<img src=%s />' % (link)
    thumb.allow_tags = True 
    
    def thumbmedium(self):
        """Get medium size thumb to display during fav restaurants"""
        link = self.get_thumbmedium_url()
        if link is None:
            return '<a href="#" target="_blank">NO IMAGE</a>'
        else:
            return '<img src=%s />' % (link)
    thumbmedium.allow_tags = True 
    
    def thumbsmall(self):
        """Get medium size thumb to display during fav restaurants"""
        link = self.get_thumbsmall_url()
        if link is None:
            return '<a href="#" target="_blank">NO IMAGE</a>'
        else:
            return '<img src=%s />' % (link)
    thumbsmall.allow_tags = True 
     
    def fullpicture(self):
        """ Get full picture <a>. """
        link = "%s%s" % (settings.MEDIA_URL, self.resart)
        if link is None:
            return '<a href="#" target="_blank">NO IMAGE</a>'
        else:
            return '<img src=%s />' % (link)
    thumb.allow_tags = True


class RestaurantFav(models.Model):
    resname = models.ForeignKey(Restaurant,verbose_name=_("favrestaurant"))
    favouritedusers = models.ManyToManyField(User)

class RestaurantRating(models.Model):
    resname = models.ForeignKey(Restaurant,verbose_name=_("resname"))
    rateduser  = models.ManyToManyField(User)
    resrating = models.CharField(_("resrating"),blank=False,max_length=5)

    def ruser(self):
        return self.rateduser

class RestaurantMenu(models.Model):
    menuitem = models.ForeignKey(Recipe,verbose_name=_("menuitem"))
    resnames = models.ManyToManyField(Restaurant)

class RestaurantChain(models.Model):
    fromres = models.ForeignKey(Restaurant,verbose_name=_("fromres"),related_name=_("fromres"))
    tores = models.ForeignKey(Restaurant,verbose_name=_("tores"))
    status = models.BooleanField(_("status"),default=False)
    added = models.DateTimeField(_('added'), default=datetime.now)
    accepted = models.DateTimeField(_('accepted'), default=datetime.now)

class MenuList(models.Model):
    res = models.ForeignKey(Restaurant,verbose_name=_("resmenlist"))
    listname = models.CharField(_('listname'), max_length=1024,default="Category")    

class MenuItem(models.Model):
    resname =  models.ForeignKey(Restaurant,verbose_name=_("resnamelist"))
    menulist = models.ForeignKey(MenuList,verbose_name=_("menulist"))
    menuitem = models.CharField(_('menuitem'), max_length=1024,default="Menu Item")    
    rating = RatingField(range=5,can_change_vote = True) # 5 possible rating values, 1-5

class RecipeSync(models.Model):
    menuitem = models.ForeignKey(MenuItem,verbose_name=_("menuitem"))
    recipe = models.ForeignKey(Recipe,verbose_name=_("recipe"))
    restaurant = models.ForeignKey(Restaurant,verbose_name=_("restaurant"))
