#from django
from random import choice
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect,HttpResponse,HttpRequest
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import csrf_exempt



#from restaurants
from restaurants.models import *
from restaurants.forms import RestaurantForm

#from recipes
from recipes.models import Recipe

#from pinax
from profiles.models import Profile

#from djangoratings
from djangoratings.views import *

from socialJSON.myJSON import *
from socialstream.models import *
from django.utils.simplejson import *

from googlemaps import GoogleMaps

def restaurants(request):
    """ Return the all restaurants list, ordered by added date. """
    restaurants = Restaurant.objects.all().order_by("-added")
    ip = request.META.get('REMOTE_ADDR', None)
    return render_to_response("restaurants/restaurants.html", {
        "location":ip,
        "restaurants": restaurants,
        "list": 'all',
    }, context_instance=RequestContext(request))

def searchrestaurants(request):
    """ Return the all restaurants list, ordered by added date. """
    if request.is_ajax and request.method=="POST":
       listrests = []
       itemrests = []
       catg = request.POST['category']
       mlist = request.POST['menulist']
       mitem = request.POST['menuitem']
       cuis = request.POST['cuisine']
       loc = request.POST['location']
       if loc=="My Location":
          longi = request.POST['longitude'] 
          lati = request.POST['latitude'] 
          gmaps = GoogleMaps()
          destination = gmaps.latlng_to_address(float(lati),float(longi))
          addr = str(destination.split(',')[0][1:])
          city = str(destination.split(',')[1][1:])
          rests = Restaurant.objects.filter(Q(address__contains=addr)|Q(city__contains=city))
       if not loc=="":
          if not loc=="My Location":
             rests = Restaurant.objects.filter(Q(address__contains=loc)|Q(city__contains=loc)|Q(state__contains=loc)).order_by("-added")          
       if loc=="":
          rests = Restaurant.objects.all().order_by("-added")
       if not catg=="Any":
          rests = rests.filter(category=catg)
       if not cuis=="":
          rests = rests.filter(cuisines__contains=cuis)
       if not mlist=="":
          for rest in rests:
              mlists = MenuList.objects.filter(listname__contains=mlist,res=rest)
              if mlists:
                 listrests.append(mlists[0].res)
          rests=list(listrests)
       if not mitem=="":
          for rest in rests:
              mitems = MenuItem.objects.filter(menuitem__contains=mitem,resname=rest)
              if mitems:
                 itemrests.append(mitems[0].resname)
          rests=list(itemrests)
       search_res = restaurants_to_jsonready(rests)
       search_data = {}
       search_data["searchres"] = search_res
       return JSONResponse(search_data)

def restaurant(request, restaurant_id):
    """ Return a restaurant given its id. """
    restaurant = Restaurant.objects.get(id=restaurant_id)
    if request.user.is_authenticated():
       isyours = False
       favbutton = False
       resfav = RestaurantFav.objects.filter(resname=restaurant,favouritedusers=request.user)
       if not resfav:
          favbutton = True
       if request.user == restaurant.adder:
          isyours = True
       return render_to_response("restaurants/restaurant.html", {
        "restaurant": restaurant,
        "isyours": isyours,
        "favbutton":favbutton, 
     
       }, context_instance=RequestContext(request))
    else:
          favbutton = True
          isyours = False
          return render_to_response("restaurants/restaurant.html",{
          "restaurant":restaurant,
          "isyours": isyours,
          "favbutton":favbutton, 
          }, context_instance=RequestContext(request))    

def myrestaurants(request,user_name):
    """ Return the logged user restaurants list. """
    user=User.objects.get(username=user_name)
    myres = Restaurant.objects.filter(adder=user).order_by("-added")
    search_res = restaurants_to_jsonready(myres)
    search_data = {}
    search_data["myrestaurants"] = search_res
    return JSONResponse(search_data)

@login_required
def add_restaurant(request): 
    #""" Add a restaurant to the database. """
    # POST request
    if request.method == "POST":
       restaurant_form = RestaurantForm(request.POST, request.FILES)
       if restaurant_form.is_valid():
        # from ipdb import set_trace; set_trace()
          new_restaurant = restaurant_form.save(commit=False)
          new_restaurant.adder = request.user
          new_restaurant.save(csrf(request))
          action = Action(actor=request.user,verb="added",action_object=2,objectid=new_restaurant.id,objectname=new_restaurant.name)
          action.save()
          request.user.message_set.create(message=_("You have saved restaurant '%(name)s'") %  {'name': new_restaurant.name})
          return HttpResponseRedirect(reverse("restaurants.views.restaurants"))
         
    # GET request
    else:
           restaurant_form = RestaurantForm()
           return render_to_response("restaurants/add.html", {
               "restaurant_form": restaurant_form,
               }, context_instance=RequestContext(request))
    # generic case
    return render_to_response("restaurants/add.html", {
       "restaurant_form": restaurant_form,
       }, context_instance=RequestContext(request)) 
 

@csrf_exempt
def edit_restaurant(request, restaurant_id):
    """ Update a restaurant given its id. """
    if request.user.is_authenticated(): 
       restaurant = Restaurant.objects.get(id=restaurant_id)
       if not request.user== restaurant.adder:
          return HttpResponseRedirect(reverse("restaurants.views.restaurants"))             
       if request.method == "POST":
          restaurant_form = RestaurantForm(request.POST, request.FILES, instance=restaurant)
          restaurant_form.is_update = True
          if request.user == restaurant.adder:
             #from ipdb import set_trace; set_trace()
             if restaurant_form.is_valid():
                restaurant_form.save()
                action = Action(actor=request.user,verb="edited",action_object=2,objectid=restaurant_id,objectname=restaurant.name)
                action.save()
                request.user.message_set.create(message=_("You have edited restaurant '%(name)s'") %  {'name': restaurant.name})
                return HttpResponseRedirect(reverse("restaurants.views.restaurants"))             
       else:
           restaurant_form = RestaurantForm(instance=restaurant)
           return render_to_response("restaurants/edit.html", {
            "restaurant_form": restaurant_form,
            "restaurant": restaurant,
            }, context_instance=RequestContext(request))  
    else:
       return HttpResponseRedirect(reverse("restaurants.views.restaurants"))          
          
@login_required
def delete_restaurant(request, restaurant_id):
    """ Delete a restaurant given its id. """
    restaurant = get_object_or_404(Restaurant, id=restaurant_id)
    if request.user == restaurant.adder:
        restaurantfavs = []
        restaurantfavs = RestaurantFav.objects.filter(resname=restaurant)
        for resfav in restaurantfavs:
            resfav.delete()
        actions = Action.objects.filter(action_object=2,objectid=restaurant_id)
        for action in actions:
            action.delete()
        restaurant.delete()
        rus = Profile.objects.get(user=request.user)
        rus.resuser = False
        request.user.message_set.create(message="Restaurant Deleted")
        
    return HttpResponseRedirect(reverse("restaurants.views.restaurants"))

@login_required
def favourite_restaurant(request,restaurant_id,user_name):
     """Adds a restaurant to the user's favorite restaurants given its id."""
     if request.is_ajax:
        restaurant = get_object_or_404(Restaurant,id=restaurant_id)
        user=User.objects.get(username=user_name)
        resfav = RestaurantFav.objects.filter(resname=restaurant,favouritedusers=user)
        if not resfav:
           resfav = RestaurantFav(resname=restaurant)
           resfav.save()
           resfav.favouritedusers.add(request.user)         
           action = Action(actor=request.user,verb="favorited",action_object=2,objectid=restaurant_id,objectname=restaurant.name)
           action.save()
        return HttpResponseRedirect(reverse('describe_restaurant',args=[restaurant_id]))

@login_required
def favremove_restaurant(request,restaurant_id,user_name):
     """ Removes a restaurant from users favourite restaurants given its id."""
     if request.is_ajax:
        restaurant = get_object_or_404(Restaurant,id=restaurant_id)
        user=User.objects.get(username=user_name)
        resfav = RestaurantFav.objects.get(resname=restaurant,favouritedusers=user)
        action = Action.objects.get(actor=user,verb="favorited",action_object=2,objectid=restaurant_id)
        action.delete()
        resfav.delete()
     return HttpResponseRedirect(reverse('describe_restaurant',args=[restaurant_id]))
    
def restaurant_detail(request,other_res):
    restaurant = get_object_or_404(Restaurant,id=other_res)
    resmenu = RestaurantMenu.objects.filter(resnames=restaurant)
    recipes = []
    for recipe in resmenu:
        recipes.append(recipe.menuitem)
    reschain = RestaurantChain.objects.filter(Q(fromres=restaurant)|Q(tores=restaurant))
    restaurants=[] 
    for tmp in reschain:
        if tmp.fromres == restaurant:   
           restaurants.append(tmp.tores)
        if tmp.tores == restaurant:
           restaurants.append(tmp.fromres)
    tmprecipes = recipes_to_jsonready(recipes)
    tmpres = restaurants_to_jsonready(restaurants)
    res_data = {}
    res_data["menurecipes"]=tmprecipes
    res_data["chainrestaurants"]=tmpres
    return JSONResponse(res_data);

def menulist(request,restaurant_id):
    if request.method =="POST" and request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id)   
       datatmp = request.POST
       data = datatmp.values()
       for temp in data:
           menul = MenuList(res=restaurant,listname=temp)
           menul.save()
       msg = "Successfully added to the menu."          
       

    else:
        msg = "Failed to add to the menu.Try again"

    return HttpResponse(msg)    

def editmenulist(request,restaurant_id):
    if request.method =="POST" and request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id)   
       datatmp = request.POST
       data = datatmp.keys()
       for temp in data:
           menul = MenuList.objects.get(res=restaurant,id=temp)
           menul.listname=datatmp[temp]
           menul.save()
       msg = "Successfully updated the menu."          
       

    else:
        msg = "Failed to save the menu"

    return HttpResponse(msg)   

def removemenulist(request,menulist_id):
    if request.is_ajax:
       ctype = ContentType.objects.get(app_label="restaurants", model="menuitem")
       menul = MenuList.objects.get(id=menulist_id)
       items = MenuItem.objects.filter(menulist=menul)
       for item in items:
           uscore = Vote.objects.filter(content_type=ctype,object_id=item.id)
           if uscore:
              for usc in uscore:
                  usc.delete()
           rscore = Score.objects.filter(content_type=ctype,object_id=item.id)
           if rscore:
              for rsc in rscore:
                  rsc.delete()
           item.delete()
           rsync = RecipeSync.objects.filter(menuitem=item)
           if rsync:
              rsync[0].delete()
       menul.delete()
       msg = "Successfully removed from the menu."          
    else:
        msg = "Failed to remove from the menu"

    return HttpResponse(msg)   

def menulistview(request,restaurant_id,user_name):
    restaurant = Restaurant.objects.get(id=restaurant_id)
    menulists = MenuList.objects.filter(res=restaurant)
    user = User.objects.filter(username=user_name)
    tmplists = menulists_to_jsonready(menulists,user)
    res_data={}
    res_data["menulists"]=tmplists
    return JSONResponse(res_data);

def editmenulistview(request,restaurant_id):
    restaurant = Restaurant.objects.get(id=restaurant_id)
    menulists = MenuList.objects.filter(res=restaurant)
    
    tmplists = editmenulists_to_jsonready(menulists)
    res_data={}
    res_data["menulists"]=tmplists
    return JSONResponse(res_data);

def menuitem(request,restaurant_id):
    if request.method =="POST" and request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id)   
       datatmp = request.POST
       keys = datatmp.keys()
       for key in keys:
           tt = datatmp[key]
           b = key.rpartition('z')[0][11:]
           listmenu = MenuList.objects.get(id=b)
           menuitem = MenuItem(resname=restaurant,menulist=listmenu,menuitem=tt)
           menuitem.save()
       msg = "Successfully added to the menu."          
    else:
        msg = "Failed to add to the menu.Try again"

    return HttpResponse(str(msg))        

def editmenuitem(request,restaurant_id):
    if request.method =="POST" and request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id)   
       datatmp = request.POST
       data = datatmp.keys()
       for temp in data:
           menui = MenuItem.objects.get(resname=restaurant,id=temp)
           menui.menuitem=datatmp[temp]
           menui.save()
       msg = "Successfully updated the menu."          
       

    else:
        msg = "Failed to save the menu"

    return HttpResponse(msg)   

def removemenuitem(request,menuitem_id):
    if request.is_ajax:
           ctype = ContentType.objects.get(app_label="restaurants", model="menuitem")
           menuitem = MenuItem.objects.get(id=menuitem_id)
           uscore = Vote.objects.filter(content_type=ctype,object_id=menuitem_id)
           if uscore:
              for usc in uscore:
                  usc.delete()
           rscore = Score.objects.filter(content_type=ctype,object_id=menuitem_id)
           if rscore:
              for rsc in rscore:
                  rsc.delete()
           rsync = RecipeSync.objects.filter(menuitem=menuitem)
           if rsync:
              rsync[0].delete()
           menuitem.delete()
           msg = "Successfully deleted from the list and menu."          
    else:
        msg = "Failed to remove from the menu"

    return HttpResponse(str(msg))        

def ratemenuitem(request,menuitem_id,user_name,rating):
    if request.is_ajax:
         itemid = menuitem_id[5:] 
         menuitem = MenuItem.objects.get(id=itemid)
         user = User.objects.get(username=user_name)
         menuitem.rating.add(score=rating, user=user,ip_address=request.META['REMOTE_ADDR'])
         msg = "Successfully rated the menuitem"          
    else:
        msg = "Failed to rate the menuitem"

    return HttpResponse(str(msg))        

def userratingmenuitem(request,restaurant_id,user_name):
    restaurant = Restaurant.objects.get(id=restaurant_id)  
    user = User.objects.get(username=user_name)
    menui = MenuItem.objects.filter(resname=restaurant)
    tmpitems = menuitemsrating_to_jsonready(menui,user)
    res_data={}
    res_data["userratings"]=tmpitems    
    return JSONResponse(res_data);  

def restaurantshuffle(request):
    """ Returns a random restaurant  """
    res = Restaurant.objects.all()
    rest = choice(res)
    rid = rest.id
    return HttpResponseRedirect(reverse('describe_restaurant',args=[rid]))

def trendingrestaurants(request):          
    """ Returns the trending restaurants """
    restaurants = []
    resfavs = RestaurantFav.objects.all()[:10]
    for resfav in resfavs:
        restaurants.append(resfav.resname)
    tres = restaurants_to_jsonready(restaurants)
    data = {}
    data["trendingrestaurants"] = tres
    return JSONResponse(data) 

def recipesync(request,restaurant_id):
    """Syncs the menuitems to the recipes available"""
    if request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id)  
       menui = MenuItem.objects.filter(resname=restaurant)
       for item in menui:
           recipes = Recipe.objects.filter(name=item.menuitem)
           if recipes:
              rsyn = RecipeSync.objects.filter(menuitem=item,recipe=recipes[0],restaurant=restaurant)
              if not rsyn:
                 rsync = RecipeSync(menuitem=item,recipe=recipes[0],restaurant=restaurant)
                 rsync.save()
       return HttpResponse("Sync completed")

def resetsync(request,restaurant_id):
    """Resets the recipesync table """
    if request.is_ajax:
       restaurant = Restaurant.objects.get(id=restaurant_id) 
       rsyn = RecipeSync.objects.filter(restaurant=restaurant)
       if rsyn:
          for rs in rsyn:
              rs.delete()
       return HttpResponse("Reset completed") 

