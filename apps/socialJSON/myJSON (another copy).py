from __future__ import division
from django.shortcuts import render_to_response, get_object_or_404,HttpResponse
from django.utils.simplejson import *
from restaurants.models import *
from recipes.models import *
from photologue.models import ImageModel
from django.contrib.contenttypes.models import ContentType

#from djangoratings
from djangoratings.views import *
from djangoratings.models import *

def recipes_to_jsonready(recipes):
    """Takes a list of recipe objects and returns a dictionary to be dumped into a JSON string"""
    count = 0
    temp_recipes = []
    for recipe in recipes:
           recipe_data= {}
           recipe_data["id"] = str(recipe.id)
           recipe_data["name"] = str(recipe.name)
           recipe_data["image"]  = str(recipe.recipeart)[8:] 
           recipe_data["category"] = str(recipe.category)
           temp_recipes.insert(count,recipe_data)
           count +=1
           

    return temp_recipes
   

def restaurants_to_jsonready(restaurants):
    """Takes a list of restaurant objects and returns a dictionary to be dumped into a JSON string"""
    count = 0
    temp_restaurants = []
    for restaurant in restaurants:
           restaurant_data= {}
           restaurant_data["id"] = str(restaurant.id)
           restaurant_data["name"] = str(restaurant.name)
           restaurant_data["image"]  = str(restaurant.resart)[12:] 
           restaurant_data["category"] = str(restaurant.category)
           temp_restaurants.insert(count,restaurant_data)
           count +=1
    return temp_restaurants 
              
def activities_to_json(activities):
    """Takes a list of social stream actions and returns a JSON string"""
    count = 0
    temp_activities = []
    for activity in activities:
           activity_data = {}
           activity_data["actor"] = str(activity.actor)
           tmp = activity.action_object
           if (tmp=='1'):
              objname = Recipe.objects.get(id=activity.objectid) 
              activity_data["objname"] = str(objname.name)
              activity_data["verb"] = str(activity.verb) + " the recipe"
              activity_data["objurl"] = "/recipes/"+str(objname.id)+"/recipe"
           elif (tmp=='2'):
              objectname = Restaurant.objects.get(id=activity.objectid)   
              activity_data["objname"] = str(objectname.name)
              activity_data["verb"] = str(activity.verb) + " the restaurant"
              activity_data["objurl"] = "/restaurants/"+str(objectname.id)+"/restaurant"
           activity_data["created"] = str(activity.timestamp)
           temp_activities.insert(count,activity_data)
           count +=1  
    stream_data={}
    stream_data["data"] = temp_activities
    data = dumps(stream_data)
    return HttpResponse(data,mimetype='application/json') 

def photos_to_jsonready(photos):
    """Takes a list of photos and returns a dictionary containing properties of the photos """
    count = 0
    temp_photos = []
    for photo in photos:
           photo_data= {}
           photo_data["id"] = str(photo.id)
           photo_data["image"]  = str(photo.get_thumbnail_url())
           photo_data["largeimage"] = str(photo._get_SIZE_url("display"))
           photo_data["title"]=str(photo.title)
           temp_photos.insert(count,photo_data)
           count +=1
    return temp_photos

def menulists_to_jsonready(menulists,user_name):
    count = 0
    temp_lists = []
    ctype = ContentType.objects.get(app_label="restaurants", model="menuitem")
    for tmp in menulists:
        res_data = {}
        res_data["id"]=str(tmp.id)
        res_data["listname"]=str(tmp.listname)
        items = MenuItem.objects.filter(menulist=tmp)
        icount = 0
        temp_items = []
        for item in items:
            item_data = {}
            item_data["id"]=str(item.id)
            item_data["itemname"] = str(item.menuitem)
            oscore = item.rating.get_rating() 
            item_data["orating"] = str(oscore)
            rscore = Score.objects.filter(content_type=ctype,object_id=item.id)
            if rscore:
               sco = rscore[0].score/rscore[0].votes
            else: 
               sco =0 
            item_data["rrating"] = str(sco)
            uscore = Vote.objects.filter(content_type=ctype,object_id=item.id,user=user_name)
            if uscore:
               scor = uscore[0].score
            else: 
               scor =0 
            item_data["urating"] = str(scor)
            temp_items.insert(icount,item_data)
            icount +=1
        res_data["menuitems"] = temp_items
        temp_lists.insert(count,res_data)
        count +=1
    return temp_lists

def editmenulists_to_jsonready(menulists):
    count = 0
    temp_lists = []
    for tmp in menulists:
        res_data = {}
        res_data["id"]=str(tmp.id)
        res_data["listname"]=str(tmp.listname)
        items = MenuItem.objects.filter(menulist=tmp)
        icount = 0
        temp_items = []
        for item in items:
            item_data = {}
            item_data["id"]=str(item.id)
            item_data["itemname"] = str(item.menuitem)
            temp_items.insert(icount,item_data)
            icount +=1
        res_data["menuitems"] = temp_items
        temp_lists.insert(count,res_data)
        count +=1
    return temp_lists

def menuitemsrating_to_jsonready(menuitems,user_name):
    count = 0 
    temp_items = []
    ctype = ContentType.objects.get(app_label="restaurants", model="menuitem")
    for tmp in menuitems:
        item_data = {}
        item_data["id"]=str(tmp.id)
        rscore = Vote.objects.filter(content_type=ctype,object_id=tmp.id,user=user_name)
        if rscore:
           sco = rscore[0].score
        else: 
           sco =0 
        item_data["urating"] = str(sco)
        temp_items.insert(count,item_data)
        count+=1
    return temp_items

def JSONResponse(dictionary):
    diction = dumps(dictionary)
    return HttpResponse(diction,mimetype='application/json')
    


