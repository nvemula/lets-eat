#from django
from django import forms
from django.utils.translation import ugettext_lazy as _

#from recipes
from recipes.models import Recipe
from django import forms
from django.utils.safestring import mark_safe

from recipes import admin

class RecipeForm(forms.ModelForm):
    """
    Recipe Form: form associated to the Recipe model
    """


    def __init__(self, *args, **kwargs):
        super(RecipeForm, self).__init__(*args, **kwargs)
        self.is_update = False
    
    def clean(self):
        """ Do validation stuff. """
        # name is mandatory
        if 'name' not in self.cleaned_data:
            return
        # if a recipe with that title already exists...
        if not self.is_update:
            if Recipe.objects.filter(name=self.cleaned_data['name']).count() > 0:
                raise forms.ValidationError(_("There is already this recipe in the database"))
        return self.cleaned_data
    
    class Meta:
        model = Recipe
        fields = ('recipeart','name', 'alsoknownas', 'recipetype','category', 'cuisine','cookingmethod','servings','ingredients','preparation', 'calories', 'mostlycookedin','taste','preferredat','video','cookingtime','preparationtime','description')

class RecipeEditForm(forms.ModelForm):
    """
    Recipe Edit Form: form associated to the Recipe model
    """
  

    def __init__(self, *args, **kwargs):
        super(RecipeEditForm, self).__init__(*args, **kwargs)
        self.is_update = False
    
    def clean(self):
        """ Do validation stuff. """
        # name is mandatory
        if 'name' not in self.cleaned_data:
            return
        # if a recipe with that title already exists...
        if not self.is_update:
            if Recipe.objects.filter(name=self.cleaned_data['name']).count() > 0:
                raise forms.ValidationError(_("There is already this recipe in the database"))
        return self.cleaned_data
    
    class Meta:
        model = Recipe
        fields = ('recipeart','name', 'alsoknownas', 'recipetype','category', 'cuisine','cookingmethod','servings','ingredients','preparation', 'calories', 'mostlycookedin','taste','preferredat','video','cookingtime','preparationtime','description')
        

