# python
import os
from os import path
from datetime import datetime

# django imports
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from profiles.models import Profile




#from djangoratings
from djangoratings.fields import RatingField


class Recipe(models.Model):
    """
    Recipe Model: name, alsoknownas, category, preparation, recipeart, adder,     added,recipetype,mostlycookedin,energylevels,health,ingredients,video,userfav,taste,cookingtime,description
      """
    RECIPETYPE_CHOICES = (
           ('Vegetarian','Vegetarian'),
           ('Non-Vegetarian','Non-Vegetarian'),
     ) 
    TASTE_CHOICES = (
           ('Spicy','Spicy'),
           ('Sweet','Sweet'),
           ('Mild','Mild'),
           ('Sour','Sour'),
           ('SweetSour','Sweet & Sour'),
           ('Tangy','Tangy'), 
    ) 
   
    MEAL_CHOICES = (
           ('Breakfast','Breakfast'),
           ('Lunch','Lunch'),
           ('Snack','Snack'),
           ('Dinner','Dinner'),
           ('Any Time','Any Time'), 
    ) 
    CATEGORY_CHOICES = (
           ('Beverages','Beverages'),
           ('Breads','Breads,Cakes'),
           ('Choco','Candy,Cookies,Chocolates'),
           ('SideDishes','Curries,Side-Dishes,Pickles,Chutneys'),
           ('DessertsSweets','Desserts & Sweets'),
           ('MainDishes','Main-Dishes,Rice'),
           ('Noodles','Noodles & Pastas'), 
           ('Salads','Salads'),
           ('Sandwiches','Pie,Pizza,Sandwiches'),
           ('Sauces','Sauces'),
           ('Snacks','Snacks'), 
           ('Soups','Soups'),
           ('Other','Other'),
    )
    COOKINGMETHOD_CHOICES = (
           ('Bake','Bake'),
           ('Blender','Blender'),
           ('Bread Machine','Bread Machine'),
           ('Broil','Broil'),
           ('Convection Oven','Convection Oven'),
           ('Fondue','Fondue'),
           ('Food Processor','Food Processor'), 
           ('Fry','Fry'),
           ('Grill','Grill'),
           ('Grill Pan','Grill Pan'),
           ('Grind','Grind'),
           ('Ice-Cream Maker','Ice-Cream Maker'), 
           ('Marinate','Marinate'),
           ('Microwave','Microwave'),
           ('Poach','Poach'),
           ('Pressure Cooker','Pressure Cooker'),
           ('Roast','Roast'),
           ('Microwave','Microwave'),
           ('Sandwich Press','Sandwich Press'),
           ('Slow Cook','Slow Cook'),
           ('Steam','Steam'),
           ('Waffle Maker','Waffle Maker'),
           
    )
    SERVINGS_CHOICES = (
           ('One','One'),
           ('Two','Two'),
           ('Three','Three'),
           ('Four','Four'),
           ('Five','Five'),
           ('Six','Six'),
           ('Seven','Seven'), 
           ('Eight','Eight'),
           ('Nine','Nine'),
           ('Ten','Ten'),
    )
    CUISINE_CHOICES = (
           ('African','African'),
           ('American','American'),
          
           ('Asian','Asian'),
           ('Austrian','Austrian'),
           ('Authentic','Authentic'),
           
           ('British','British'),
           ('Caribbean','Caribbean'),
            ('Chinese','Chinese'),
           ('Cuban','Cuban'),
           ('Eastern European','Eastern European'),
           ('French','French'),
           ('German','German'),
             ('Greek','Greek'),
             ('Hawaiian','Hawaiian'),
             ('Hungarian','Hungarian'),
            ('Andhra','Indian - Andhra'),
             ('Awadhi','Indian - Awadhi'),
           ('Bengali','Indian - Bengali'),
           ('Bihari','Indian - Bihari'),
            ('Goan','Indian - Goan'),
           ('Gujarati','Indian - Gujarati'), 
            ('Hyderabadi','Indian - Hyderabadi'),
           ('Jain','Indian - Jain'),
           ('Kashmiri','Indian - Kashmiri'),
           ('Maharashtrian','Indian - Maharashtrian'),
           ('Parsi','Indian - Parsi'),
           ('Punjabi','Indian - Punjabi'),
           ('Rajasthani','Indian - Rajasthani'),
           ('Sindhi','Indian - Sindhi'),
           ('South Indian','Indian - South Indian'),
            ('Italian','Italian'),
           ('Japanese','Japanese'),
            ('Jewish','Jewish'),
           ('Korean','Korean'),
            ('Malaysian','Malaysian'),
            ('Mediterranean','Mediterranean'),
             ('Mexican','Mexican'),
              ('Middle Eastern','Middle Eastern'),
             ('Moroccan','Moroccan'),
             ('Pacific Northwest','Pacific Northwest'),
             ('Scandinavian','Scandinavian'),
             ('South American','South American'), 
             ('Spanish','Spanish'),
             ('Swiss','Swiss'),
              ('Thai','Thai'), 
              ('Vietnamese','Vietnamese'),
            
     
     )
    COOKINGTIME_CHOICES = (
           ('0 to 5 Min','0 to 5 Minutes'),
           ('5 to 10 Min','5 to 10 Minutes'),
           ('10 to 15 Min','10 to 15 Minutes'),
           ('15 to 30 Min','15 to 30 Minutes'),
           ('30 Min to 1 Hour','30 Min to 1 Hour'),
           ('More than 1 Hr','More than 1 Hour'),
    )
    PREPARATION_CHOICES = (
           ('5 to 10 Min','5 to 10 Minutes'),
           ('10 to 15 Min','10 to 15 Minutes'),
           ('15 to 30 Min','15 to 30 Minutes'),
           ('30 Min to 1 Hour','30 Min to 1 Hour'),
           ('More than 1 Hr','More than 1 Hour'),
           ('More than 3 Hrs','More than 3 Hrs'),
           ('6 to 8 Hrs','6 to 8 Hours'),
           ('Half Day','Half Day'),
           ('1 Dy','1 Day'), 
           ('More than 1 Dy','More than 1 Day'),
           ('3 to 4 Days','3 to 4 Days'),
           ('A Week','A Week'),
    )
    name = models.CharField(_('Name'), max_length=255)
    alsoknownas = models.CharField(_('Also Known As'), max_length=255)
    category = models.CharField(_('Category'), max_length=255,null=False,choices=CATEGORY_CHOICES,default="Beverages")
    cuisine = models.CharField(_('Cuisine'), max_length=255,choices = CUISINE_CHOICES,default="Hyderabadi")
    preparation = models.TextField(_('Preparation'), blank=True)
    recipeart = models.ImageField(_('Recipe Art'),upload_to="recipes", blank=True, null=True) 
    recipetype = models.CharField(_('Recipe Type'), max_length=255,choices=RECIPETYPE_CHOICES,default="Vegetarian")
    mostlycookedin = models.CharField(_('Mostly Cooked In'), max_length=255)
    energylevels = models.CharField(_('energylevels'), max_length=255)
    taste = models.CharField(_('Taste'), max_length=255,choices=TASTE_CHOICES,default="Spicy")
    preferredat = models.CharField(_('Preferred At'), max_length=255,choices=MEAL_CHOICES,default="Lunch")
    ingredients = models.TextField(_('Ingredients'), blank=False)
    video = models.URLField(_('Video'), max_length=255, blank=True)
    calories = models.CharField(_('Calories'), max_length=255)
    preparationtime = models.CharField(_('Preparation Time'), max_length=255,choices = PREPARATION_CHOICES,default="5 to 10 Min")
    cookingtime = models.CharField(_('Cooking Time'), max_length=255,choices = COOKINGTIME_CHOICES,default="0 to 5 Min")
    cookingmethod = models.CharField(_('Cooking Method'), max_length=255,choices=COOKINGMETHOD_CHOICES,default="Bake")
    servings = models.CharField(_('Servings '), max_length=255,choices=SERVINGS_CHOICES,default="One")
    description = models.TextField(_('Description'),blank=True)
    
    rating = RatingField(range=5,can_change_vote = True) # 5 possible rating values, 1-5
    adder = models.ForeignKey(User, related_name="added_recipes", verbose_name=_('adder'))
    added = models.DateTimeField(_('added'), default=datetime.now)

    def get_absolute_url(self):
        return ("describe_recipe", [self.pk])
    get_absolute_url = models.permalink(get_absolute_url)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ('-added', )
        
    def _get_thumb_url(self, folder, size):
        """ get a thumbnail giver a folder and a size. """
        if not self.recipeart:
            return '#'       
        upload_to = path.dirname(self.recipeart.path)
        tiny = path.join(upload_to, folder, path.basename(self.recipeart.path))
        tiny = path.normpath(tiny)
        if not path.exists(tiny):
            import Image
            im = Image.open(self.recipeart.path)
            im.thumbnail(size, Image.ANTIALIAS)
            im.save(tiny, 'JPEG')  
        return path.join(path.dirname(self.recipeart.url), folder, path.basename(self.recipeart.path)).replace('\\', '/')

    def get_thumb_url(self):
        return self._get_thumb_url('thumb_400_400', (400,400))

    def get_thumbmedium_url(self):
        return self._get_thumb_url('thumb_100_100', (100,100))
    
    def thumb(self):
        """ Get thumb <a>. """
        link = self.get_thumb_url()
        if link is None:
            return '<a href="#" target="_blank">NO IMAGE</a>'
        else:
            return '<img src=%s />' % (link)
    thumb.allow_tags = True 
    
    def thumbmedium(self):
        """Get medium size thumb to display during fav,suggested and uploaded recipes"""
        link = self.get_thumbmedium_url()
        if link is None:
            return '<a href="#" target="_blank">NO IMAGE</a>'
        else:
            return '<img src=%s />' % (link)
    thumbmedium.allow_tags = True 
         
    def fullpicture(self):
        """ Get full picture <a>. """
        link = "%s%s" % (settings.MEDIA_URL, self.recipeart)
        if link is None:
            return '<a href="#" target="_blank">NO IMAGE</a>'
        else:
            return '<img src=%s />' % (link)
    thumb.allow_tags = True
    
    __original_image = None
 
    def __init__(self, *args, **kwargs):
        super(Recipe, self).__init__(*args, **kwargs)
        self.__original_image = self.recipeart
   
    def save(self, force_insert=False, force_update=False):
        if self.recipeart != self.__original_image and self.__original_image != '':
           self.delete_images()

        super(Recipe, self).save(force_insert, force_update)
        self.__original_image = self.recipeart
 
    def delete_images(self, empty_image=False):
        image_path = os.path.join(settings.MEDIA_ROOT, str(self.__original_image))
 
        try:
            os.remove(image_path)
            self.main_image.delete()
        except:
            pass
 
        if empty_image:
            self.image = ''
    
class RecipeFav(models.Model):
    recipename = models.ForeignKey(Recipe,verbose_name=_("favrecipe"))
    favouritedusers =  models.ManyToManyField(User)

class RecipeRating(models.Model):
    recipename = models.ForeignKey(Recipe,verbose_name=_("recipename"))
    rateduser  = models.ManyToManyField(User)
    reciperating = models.CharField(_("reciperating"),blank=False,max_length=5)

    def ruser(self):
        return self.rateduser

class SuggestedRecipes(models.Model):
    recipename = models.ForeignKey(Recipe,verbose_name=_("sugrecipe"))
    suggestedto =  models.ManyToManyField(User)




