#from django
from django.conf.urls.defaults import *

# from recipes
from recipes.models import Recipe

urlpatterns = patterns('',
        url(r'^$', 'recipes.views.recipes', name="recipes"),
                 url(r'^searchrecipes/$', 'recipes.views.searchrecipes', name="searchrecipes"),
        url(r'^(\d+)/recipe/$', 'recipes.views.recipe', name="describe_recipe"),
        url(r'^your_recipes/$', 'recipes.views.your_recipes', name="your_recipes"),
        url(r'^myrecipes/(?P<user_name>[\w\._-]+)/$', 'recipes.views.myrecipes', name="myrecipes"),
        # CRUD urls
        url(r'^upload/$', 'recipes.views.upload_recipe', name="upload_recipe"),
        url(r'^(\d+)/update/$', 'recipes.views.update_recipe', name="update_recipe"),
         url(r'^(\d+)/updateimg/$', 'recipes.views.update_recipeimg', name="update_recipeimg"),
        url(r'^(\d+)/delete/$', 'recipes.views.delete_recipe', name="delete_recipe"),
        url(r'^favourite/(?P<recipe_id>\w+)/(?P<user_name>[\w\._-]+)/$', 'recipes.views.favourite_recipe', name="favourite_recipe"),
        url(r'^favremove/(?P<recipe_id>\w+)/(?P<user_name>[\w\._-]+)/$','recipes.views.favremove_recipe', name="favremove_recipe"),
        url(r'^raterecipe/(?P<recipe_id>\w+)/(?P<user_name>[\w\._-]+)/(?P<rating>\w+)/$', 'recipes.views.raterecipe', name="raterecipe"), 
        url(r'^(\d+)/rate_recipe/$','recipes.views.rate_recipe', name="rate_recipe"),   
        url(r'^suggest/(?P<recipe_category>\w+)/$','recipes.views.suggest_recipes', name="suggest_recipes"),
        url(r'^viewsugrecipes/(?P<user>[\w\._-]+)/$','recipes.views.viewsugrecipes', name="viewsugrecipes"),  
        url(r'^selectcategory/$','recipes.views.select_sug_recipes', name="select_sug_recipes"),   
        url(r'^profiledisp_data/(?P<other_user>[\w\._-]+)/$', 'recipes.views.profiledisp_data', name="profiledisp_data"),
        url(r'^recipeshuffle/$', 'recipes.views.recipeshuffle', name="recipeshuffle"),
        url(r'^trendingrecipes/$', 'recipes.views.trendingrecipes', name="trendingrecipes"),
)

