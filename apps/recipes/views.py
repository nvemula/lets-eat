#from django
from random import choice
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect



#from pinax
from profiles.models import Profile
from photos.models import Image, Pool

#from recipes
from recipes.models import *
from recipes.forms import *


#from restaurants
from restaurants.models import Restaurant,RestaurantMenu

#from crab
from scikits.crab.models.classes import MatrixPreferenceDataModel
from scikits.crab.recommenders.knn.classes import ItemBasedRecommender
from scikits.crab.similarities.basic_similarities import ItemSimilarity
from scikits.crab.recommenders.knn.item_strategies import ItemsNeighborhoodStrategy
from scikits.crab.metrics.pairwise import euclidean_distances

#from djangoratings
from djangoratings.views import *
from djangoratings.models import *

from socialJSON.myJSON import *
from socialstream.models import Action

def recipes(request):
    """ Return the all recipes list, ordered by added date. """
    recipes = Recipe.objects.all().order_by("-added")
    return render_to_response("recipes/recipes.html", {
        "recipes": recipes,
        "list": 'all',
    }, context_instance=RequestContext(request))
    
def searchrecipes(request):
    """ Return recipes based on search filters, ordered by added date. """
    if request.is_ajax and request.method=="POST":
       catg = request.POST['category']
       if not catg=="Any":
          q = Recipe.objects.filter(category=catg).order_by("-added")
       else:
          q = Recipe.objects.all().order_by("-added")
       meal = request.POST['meal']
       if not meal=="Any":
          q = q.filter(meal=meal)
       cuisine = request.POST['cuisine']
       if not cuisine=="Any":
          q = q.filter(cuisine=cuisine)
       ckmd = request.POST['cookingmethod']
       if not ckmd=="Any":
          q = q.filter(cookingmethod=ckmd)
       taste = request.POST['taste']
       if not taste=="Any":
          q = q.filter(taste=taste)
       servings = request.POST['servings']
       if not servings=="Any":
          q = q.filter(servings=servings)
       ptime = request.POST['preparationtime']
       if not ptime=="Any":
          q = q.filter(preparationtime=ptime)
       ctime = request.POST['cookingtime']
       if not ctime=="Any":
          q = q.filter(cookingtime=ctime)
    ser_recipes = recipes_to_jsonready(q)    
    ser_data = {}
    ser_data["searchrecipes"]=ser_recipes
    return JSONResponse(ser_data)

def recipe(request, recipe_id):
    """ Return a recipe given its id. """
    score=0
    recipe = Recipe.objects.get(id=recipe_id)
    ctype = ContentType.objects.get(app_label="recipes", model="recipe")
    if request.user.is_authenticated():
       isyours = False
       favbutton = False
       ratingform = False
       rscore = Vote.objects.filter(content_type=ctype,object_id=recipe_id,user=request.user)
       if rscore:
          score = rscore[0].score
       else: 
          score = 0 
       recipefav = RecipeFav.objects.filter(recipename=recipe,favouritedusers=request.user)
       if not recipefav:
          favbutton = True
       if request.user == recipe.adder:
          isyours = True
       return render_to_response("recipes/recipe.html", {
        "recipe": recipe,
        "isyours": isyours,
        "favbutton":favbutton, 
        "score":score,
        }, context_instance=RequestContext(request))
    else:
          favbutton = True
          isyours = False
          score =   recipe.rating.get_rating()  
          return render_to_response("recipes/recipe.html",{
          "recipe":recipe,
          "isyours": isyours,
          "favbutton":favbutton, 
          "score":score,
          }, context_instance=RequestContext(request))  

@login_required
def your_recipes(request):
    """ Return the logged user recipes list. """
    yourrecipes = Recipe.objects.filter(adder=request.user).order_by("-added")
    return render_to_response("recipes/your_recipes.html", {
        "recipes": yourrecipes,
        "list": 'yours',
    }, context_instance=RequestContext(request))   

def myrecipes(request,user_name):
    """ Return the logged user recipes list. """
    user=User.objects.get(username=user_name)
    myrecipes = Recipe.objects.filter(adder=user).order_by("-added")
    ser_recipes = recipes_to_jsonready(myrecipes)    
    ser_data = {}
    ser_data["myrecipes"]=ser_recipes
    return JSONResponse(ser_data)

@login_required
def upload_recipe(request): 
    #""" Upload a recipe to the database. """
    # POST request
    if request.method == "POST":
        recipe_form = RecipeForm(request.POST, request.FILES)
        if recipe_form.is_valid():
            # from ipdb import set_trace; set_trace()
            new_recipe = recipe_form.save(commit=False)
            new_recipe.adder = request.user
            new_recipe.save()
            action = Action(actor=request.user,verb="uploaded",action_object=1,objectid=new_recipe.id,objectname=new_recipe.name)
            action.save(csrf(request))
            request.user.message_set.create(message=_("You have saved recipe '%(name)s'") %  {'name': new_recipe.name})
            return HttpResponseRedirect(reverse("recipes.views.recipes"))
    # GET request
    else:
        recipe_form = RecipeForm()
        return render_to_response("recipes/upload.html", {
            "recipe_form": recipe_form,
            }, context_instance=RequestContext(request))

    
    # generic case
    return render_to_response("recipes/upload.html", {
        "recipe_form": recipe_form,
    }, context_instance=RequestContext(request)) 
    
@login_required
def update_recipe(request, recipe_id):
    """ Update a recipe given its id. """
    recipe = Recipe.objects.get(id=recipe_id)
    if not request.user==recipe.adder:
       return HttpResponseRedirect(reverse('recipes'))
    if request.method == "POST":
       recipe_form = RecipeEditForm(request.POST, request.FILES, instance=recipe)
       recipe_form.is_update = True
       if request.user == recipe.adder:
            #from ipdb import set_trace; set_trace()
          if recipe_form.is_valid():
             recipe_form.save()
             action = Action(actor=request.user,verb="updated",action_object=1,objectid=recipe_id,objectname=recipe.name)
             action.save()
             request.user.message_set.create(message=_("You have updated recipe '%(name)s'") %  {'name': recipe.name})
             return HttpResponseRedirect(reverse('describe_recipe',args=[recipe_id]))
    else:
        recipe_form = RecipeEditForm(instance=recipe)
        return render_to_response("recipes/update.html", {
            "recipe_form": recipe_form,
            "recipe": recipe,
            }, context_instance=RequestContext(request))  

@login_required
def update_recipeimg(request, recipe_id):
    """ Update a recipe given its id. """
    recipe = Recipe.objects.get(id=recipe_id)
    if request.method == "POST":
       recipe_form = RecipeForm(request.FILES, instance=recipe)
       recipe_form.is_update = True
       if request.user == recipe.adder:
            #from ipdb import set_trace; set_trace()
          if recipe_form.is_valid():
             recipe_form.save()
             action = Action(actor=request.user,verb="updated",action_object=1,objectid=recipe_id,objectname=recipe.name)
             action.save()
             return HttpResponseRedirect(reverse('describe_recipe',args=[recipe_id]))
    else:
        recipe_form = RecipeForm(instance=recipe)
        return render_to_response("recipes/updateimg.html", {
            "recipe_form": recipe_form,
            "recipe": recipe,
            }, context_instance=RequestContext(request))  
              
@login_required
def delete_recipe(request, recipe_id):
    """ Delete a recipe given its id. """
    recipe = get_object_or_404(Recipe, id=recipe_id)
    if request.user == recipe.adder:
        ctype = ContentType.objects.get(app_label="recipes", model="recipe")
        uscore = Vote.objects.filter(content_type=ctype,object_id=recipe_id)
        for usc in uscore:
            usc.delete()
        rscore = Score.objects.filter(content_type=ctype,object_id=recipe_id)
        for rsc in rscore:
            rsc.delete()
        recipefavs = []
        recipefavs = RecipeFav.objects.filter(recipename=recipe)
        for recipefav in recipefavs:
            recipefav.delete()
        action = Action.objects.filter(action_object=1,objectid=recipe_id)
        for act in action:
            act.delete()
        recipe.delete()
        request.user.message_set.create(message="Recipe Deleted")
        
    return HttpResponseRedirect(reverse('home'))

@login_required
def favourite_recipe(request,recipe_id,user_name):
     """Adds a recipe to the user's favorite recipes given its id."""
     if request.is_ajax:
        recipe = get_object_or_404(Recipe,id=recipe_id)
        user = User.objects.get(username=user_name)
        recipefav = RecipeFav.objects.filter(recipename=recipe,favouritedusers=user)
        if not recipefav:
           recipefav = RecipeFav(recipename=recipe)
           recipefav.save()
           recipefav.favouritedusers.add(user)         
           action = Action(actor=request.user,verb="favorited",action_object=1,objectid=recipe_id,objectname=recipe.name)
           action.save()
     return HttpResponse("Successfully added")

@login_required
def favremove_recipe(request,recipe_id,user_name):
     """ Removes a recipe from users favourite recipes given its id."""
     if request.is_ajax:
        recipe = get_object_or_404(Recipe,id=recipe_id)
        user = User.objects.get(username=user_name)
        recipefav = RecipeFav.objects.get(recipename=recipe,favouritedusers=user)
        action = Action.objects.get(verb="favorited",action_object=1,objectid=recipe_id,actor=user)
        action.delete()
        recipefav.delete()
     return HttpResponse("Successfully removed")

def raterecipe(request,recipe_id,user_name,rating):
    if request.is_ajax:
       recipe = Recipe.objects.get(id=recipe_id)
       user = User.objects.get(username=user_name)
       recipe.rating.add(score=rating, user=user,ip_address=request.META['REMOTE_ADDR'])
       msg = "Successfully rated the menuitem"          
    else:
        msg = "Failed to rate the menuitem"

    return HttpResponse(str(msg))   

@login_required
def rate_recipe(request,recipe_id):#"""Assigns a rating to a recipe when a user rates """
       if request.method =="POST":
       #   profile = Profile.objects.get(user=request.user
          recipe = get_object_or_404(Recipe,id=recipe_id)
          reciperate = RecipeRating(recipename = recipe)
          reciperate.save()
          reciperate.reciperating = request.POST["reciperating"]
          reciperate.rateduser.add(request.user)
          reciperate.save()
       return HttpResponseRedirect(reverse('describe_recipe',args=[recipe_id]))

@login_required
def select_sug_recipes(request):#"""Displays the categories for the user to select"""
       return render_to_response("recipes/selectcategory.html",context_instance=RequestContext(request))

@login_required
def suggest_recipes(request,recipe_category):#"""Suggests the recipes to the user"""
   # return HttpResponse(str(recipe_category)) 
    ctype = ContentType.objects.get(app_label="recipes", model="recipe")
    allrecipes = []
    profile = Profile.objects.get(user = request.user) 
    category = recipe_category
    if profile.foodhabits=="Vegetarian":
       allrecipes = Recipe.objects.filter(category=category,recipetype="Vegetarian")
    else:
       allrecipes = Recipe.objects.filter(category=category)
    recipe_data = {}
    recipes = []
    reciperate=[]
    for rpv in allrecipes:
        reciperate = Vote.objects.filter(content_type=ctype,object_id=rpv.id)
        if reciperate:
           for temp in reciperate:
               rname = str(temp.object_id)
               usr = temp.user
               user=usr.username
               if user not in recipe_data:
                  recipe_data[user] = {}
                  recipe_data[user][rname] = float(str(temp.score))
               else:
                  recipe_data[user][rname] = float(str(temp.score))
           if request.user.username not in recipe_data:
              recipe_data[request.user.username] = {}
    
    if recipe_data:
            model = MatrixPreferenceDataModel(recipe_data)
            items_strategy = ItemsNeighborhoodStrategy()
            similarity = ItemSimilarity(model, euclidean_distances)
            recsys = ItemBasedRecommender(model, similarity, items_strategy)
           #Return the recommendations for the given user.
            recipesug = recsys.recommend(request.user.username)
            for rpe in recipesug:
                for tmp in allrecipes:
                    if tmp.id == int(rpe): 
                       recipes.append(tmp) 
                       srecipes = SuggestedRecipes.objects.filter(recipename=tmp,suggestedto=request.user)
                       if not srecipes:
                              srecipe = SuggestedRecipes(recipename=tmp)
                              srecipe.save()
                              srecipe.suggestedto.add(request.user)   
            return render_to_response("recipes/suggestions.html", {
            "recipes": recipes,
            "category": recipe_category,
            }, context_instance=RequestContext(request)) 
    else:
        return render_to_response("recipes/suggestions.html",{
            "result":"based on",
         },context_instance=RequestContext(request))  

def viewsugrecipes(request,user):
    user=User.objects.get(username=user)
    srecipes = SuggestedRecipes.objects.filter(suggestedto=user)
    recipes = []
    for recipe in srecipes:
        recipes.append(recipe.recipename)
    sug_recipes = recipes_to_jsonready(recipes)    
    sug_data = {}
    sug_data["suggested"]=sug_recipes
    return JSONResponse(sug_data)

def profiledisp_data(request, other_user):
    """ Return a user's recipes, favrecipes,restaurants & favrestaurants. """
    other_user = get_object_or_404(User, username=other_user)
    profile = Profile.objects.get(user = other_user)
    recipefavs = RecipeFav.objects.filter(favouritedusers=profile.user_id)
    recipes = []
    for recipefav in recipefavs:
        recipes.append(recipefav.recipename)
    fav_recipes = recipes_to_jsonready(recipes)
    userrecipes = Recipe.objects.filter(adder=other_user)
    usrrecipes = recipes_to_jsonready(userrecipes)
    usrrestaurants = Restaurant.objects.filter(adder=other_user)
    usrres = restaurants_to_jsonready(usrrestaurants)
    restaurants = []
    resfavs = RestaurantFav.objects.filter(favouritedusers=other_user)
    for resfav in resfavs:
        restaurants.append(resfav.resname)
    fav_res = restaurants_to_jsonready(restaurants)
    photos = Image.objects.filter(member=other_user)
    tmpphotos = photos_to_jsonready(photos)
    profile_data = {}
    profile_data["favrecipes"] = fav_recipes 
    profile_data["userrecipes"] = usrrecipes
    profile_data["userrestaurants"] = usrres
    profile_data["favrestaurants"] = fav_res
    profile_data["photos"] = tmpphotos
    return JSONResponse(profile_data)

def recipeshuffle(request):
    """ Returns a random recipe  """
    recipes = Recipe.objects.all()
    recipe = choice(recipes)
    rid = recipe.id
    return HttpResponseRedirect(reverse('describe_recipe',args=[rid]))

def trendingrecipes(request):
    recipes = []
    recipefavs = RecipeFav.objects.all()[:10]
    recipes = []
    for recipefav in recipefavs:
        recipes.append(recipefav.recipename)
    trecipes = recipes_to_jsonready(recipes)
    data = {}
    data["trendingrecipes"] = trecipes
    return JSONResponse(data)
