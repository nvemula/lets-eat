import datetime
from haystack import indexes
from recipes.models import Recipe

class RecipeIndex(indexes.SearchIndex,indexes.Indexable):
    text = indexes.CharField(use_template=True,document=True)
    name = indexes.CharField(model_attr='name')
    alsoknownas = indexes.CharField(model_attr='alsoknownas')
    category = indexes.CharField(model_attr='category')
    ingredients = indexes.CharField(model_attr='ingredients')
    preparationtime = indexes.CharField(model_attr='preparationtime')
    cookingtime = indexes.CharField(model_attr='cookingtime')
    description = indexes.CharField(model_attr='description')
    cuisine = indexes.CharField(model_attr='cuisine',null=True)

    def get_model(self):
        return Recipe
    def index_queryset(self):
        "Used when the entire index for model is updated."
        return self.get_model().objects.filter(added__lte=datetime.datetime.now())


