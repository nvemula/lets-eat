from django.contrib import admin
from recipes.models import Recipe

from django.contrib.admin.widgets import AdminFileWidget
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe

import os
import Image

class AdminImageFieldWithThumbWidget(AdminFileWidget): 
    def render(self, name, value, attrs=None): 
        thumb_html = '' 
        if value and hasattr(value, "url"): 
            thumb_html = '<img src="%s" width="60" width="60"/>' % value.url 
        return mark_safe("%s%s" % (thumb_html, 
super(AdminImageFieldWithThumbWidget, self).render(name, value, 
attrs))) 


class RecipeAdmin(admin.ModelAdmin):
    #list_display = ('name', 'alsoknownas', 'preparation', 'added', 'adder', 'recipeart')
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'recipeart':
            from django import forms 
            return 
            forms.CharField(widget=AdminImageFieldWithThumbWidget(**kwargs)) 
            return 
        super(RecipeAdmin,self).formfield_for_dbfield(db_field,**kwargs) 




				


admin.site.register(Recipe, RecipeAdmin)




