from django.conf.urls.defaults import *



urlpatterns = patterns("",
    url(r"^$", "friends.views.friends", name="invitations"),
   # url(r"^frienddecline/(?P<invitation_id>\w+)/(?P<user_name>\w+)/$", "friends.views.frienddecline", name="frienddecline"),
    #url(r"^contacts/$", "friends_app.views.contacts",  name="invitations_contacts"),
    url(r"^accept/(\w+)/$", "friends.views.accept_join", name="friends_accept_join"),
    url(r"^invite/$", "friends.views.invite", name="invite_to_join"),
)
