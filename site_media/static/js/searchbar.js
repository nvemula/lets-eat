$(document).ready(function() {

//Declare the elements we are going to use as variables
var input = $('.input');
	searchbar = $('.searchbar');
	searchbutton = $('.searchButton');

//On Mouseover of the searchbar, add the class 'searchbarhover' to '.searchbar' and 'searchButtonHover' to '.searchButton'
searchbar.mouseover(
  function () {
    searchbar.addClass('searchbarHover');
	searchbutton.addClass('searchButtonHover');
  });
  
//On Mouseout of the searchbar, remove the class 'searchbarhover' to '.searchbar' and 'searchButtonHover' to '.searchButton'  
searchbar.mouseout(
  function () {
    searchbar.removeClass('searchbarHover');
	searchbutton.removeClass('searchButtonHover');
  });

//On focus of '.input' add the class 'searchbarfocus' to '.searchbar' and 'searchButtonHover' to '.searchButton'  
input.focus(function() {
	
	searchbar.addClass('searchbarFocus');
	searchbutton.addClass('searchButtonFocus');
});

//On Blur of '.input' remove the class 'searchbarfocus' to '.searchbar' and 'searchButtonHover' to '.searchButton'  
input.blur(function() {
	
	searchbar.removeClass('searchbarFocus')
	searchbutton.removeClass('searchButtonFocus');

});

});

